package me.benana.basecore;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import me.benana.basecore.basekit.BaseKitCore;
import me.benana.basecore.economy.VaultEconomy;
import me.benana.basecore.events.DebugEvent;
import me.benana.basecore.events.EnableServerEvent;
import me.benana.basecore.exceptions.ReflectionException;
import me.benana.basecore.information.ConfigSaver;
import me.benana.basecore.information.SQLSaver;
import me.benana.basecore.information.Saver;
import me.benana.basecore.information.configurable.ConfigurableCore;
import me.benana.basecore.utils.BukkitUtils;
import me.benana.basecore.utils.PlayerUtils;
import me.benana.basecore.utils.reflection.FieldUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

/**
 * BaseCore main features.
 * 
 * @author DrBenana
 */
public class BaseCore {
	private static boolean debug = false;
	private static CommandSender defaultsender = Bukkit.getConsoleSender();
	private static boolean ranFirst = false;
	public static HashMap<UUID, ChannelPipeline> pipelines = new HashMap<>();
	public static HashMap<UUID, PacketHandler> handlers = new HashMap<>();
	
	/**
	 * Runs first. You can't use it.
	 */
	public static void runFirst() {
		// Checks run first
		if (ranFirst) {
			Main.getMessageCore().send("You can't run 'runFirst' more than 1 time.", Bukkit.getConsoleSender());
			return;
		}
		
		// EnableServerEvent
		new BukkitRunnable() {	
			@Override
			public void run() {
				Main.getPlugin().getServer().getPluginManager().callEvent(new EnableServerEvent());
			}
		}.runTaskLater(Main.getPlugin(), 0);
		
		// Register Events
		BukkitUtils.registerEvents(Main.getPlugin(), new BaseKitCore(), new BaseCoreListener(), new PlayerListener(), new ConfigurableCore());
		
		// Creates default config
		Main.getPlugin().getConfig().options().copyDefaults(true);
		Main.getPlugin().saveConfig();
		Main.getPlugin().reloadConfig();
		
		// Check default values
		if (Main.getPlugin().getConfig().getBoolean("Debug")) enableDebug();
		
		// Economies
		VaultEconomy.setupEconomy();
	}
	
	/**
	 * Enables the debug mode on BaseCore.
	 */
	public static void enableDebug() {
		debug = true;
	}
	
	/**
	 * Disables the debug mode on BaseCore.
	 */
	public static void disableDebug() {
		debug = false;
	}
	
	/**
	 * @param getter The sender who gets the debug
	 */
	public static void setDefaultGetter(CommandSender getter) {
		defaultsender = getter;
	}
	
	/**
	 * Debugs. Sends message to the senders if debug mode is enabled.
	 * @param msg The message to send.
	 * @param senders The senders who get the message. If the length of the senders is 0, It sends the message to the default sender.
	 */
	public static void debug(String msg, CommandSender... senders) {
		if (senders.length == 0) {
			debug(msg, defaultsender);
			return;
		}
		if (debug && !(new DebugEvent(msg, senders).isCancelled())) {
			for (CommandSender sender : senders) sender.sendMessage("�c�l[DEBUG MODE] �r" + msg.replaceAll("&", "�"));
		}
	}

	/**
	 * Debugs an object.
	 * @param obj The object to debug.
	 * @param senders The senders who get the message. If the length of the senders is 0, It sends the message to the default sender.
	 */
	public static <T> void debug(T obj, CommandSender... senders) {
		debug(obj.getClass().getSimpleName() + " equals &l[TO STRING]&r " + obj.toString(), senders);
	}

	/**
	 * Gets the default saver of BaseCore. Recommended to use.
	 * @param name The name of the file / section.
	 * @param plugin The JavaPlugin (Main) of your plugin.
	 * @return Default Saver via the configuration.
	 */
	public static Saver getDefaultSaver(String name, JavaPlugin plugin) {
		// Creates a saver
		// TODO Add an option to create new savers by the API.
		if (!Main.getPlugin().getConfig().getString("Configuration.Type").equalsIgnoreCase("SQL")) return new ConfigSaver(plugin, name);
		else {
			try { // Tries SQL
				return new SQLSaver(Main.getPlugin().getConfig().getString("Configuration.SQL.Host"),
						Main.getPlugin().getConfig().getString("Configuration.SQL.Port"),
						Main.getPlugin().getConfig().getString("Configuration.SQL.Database"),
						Main.getPlugin().getConfig().getString("Configuration.SQL.Username"),
						Main.getPlugin().getConfig().getString("Configuration.SQL.Password"), name);
			} catch (ClassNotFoundException | IOException | SQLException e) {
				Main.getMessageCore().sendDefault("SaveCancelled", Bukkit.getConsoleSender());
				return new ConfigSaver(plugin, name);
			}
		}
	}
	
	public static void createPlayerPacketChannel(Player p) {
		PlayerUtils.setRealName(p, p.getName());
		try {
			Channel field = null;
			if (FieldUtils.hasDeclaredField(NMSUtils.getNMS("EntityPlayer"), "playerConnection.networkManager.i", "Channel"))
				field = (Channel) FieldUtils.getDeclaredField(NMSUtils.asNMSObject(p), "playerConnection.networkManager.i");
			else
				field = (Channel) FieldUtils.getDeclaredField(NMSUtils.asNMSObject(p), "playerConnection.networkManager.channel");
			handlers.put(p.getUniqueId(), new PacketHandler(p));
			pipelines.put(p.getUniqueId(), field.pipeline());
			pipelines.get(p.getUniqueId()).addBefore("packet_handler", "BaseCorePacketHandler" + (new SimpleDateFormat("ddMMhhmmss")).format(new Date()), handlers.get(p.getUniqueId()));
		} catch (IllegalArgumentException | SecurityException e1) {
			throw new ReflectionException(e1);
		}
	}
}

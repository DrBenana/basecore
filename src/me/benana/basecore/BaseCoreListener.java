package me.benana.basecore;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

import me.benana.basecore.events.ReloadServerEvent;
import me.benana.basecore.events.StopServerEvent;

public class BaseCoreListener implements Listener {
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onCommand(ServerCommandEvent e) {
		if (e.getCommand().equals("reload")) 
			Main.getPlugin().getServer().getPluginManager().callEvent(new ReloadServerEvent());
		
		if (e.getCommand().equals("stop")) 
			Main.getPlugin().getServer().getPluginManager().callEvent(new StopServerEvent());
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().equals("/reload"))
			Main.getPlugin().getServer().getPluginManager().callEvent(new ReloadServerEvent());
		
		if (e.getMessage().equals("/stop")) 
			Main.getPlugin().getServer().getPluginManager().callEvent(new StopServerEvent());
	}
}

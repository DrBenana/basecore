package me.benana.basecore;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.basecore.general.MessageCore;
import me.benana.basecore.information.configurable.ConfigurableCore;

public class Main extends JavaPlugin {
	private static Main plugin;
	private static MessageCore mc;
	
	@Override
	public void onEnable() {
		plugin = this;
		mc = new MessageCore("�c�l[BaseCore] �r�lHey! �r");
		mc.addDefault("InvalidAnnotation", "One of the plugins that your server uses, has done a problem with the annotations system. The last event was cancelled.");
		mc.addDefault("SaveCancelled", "You've not editted well the 'Configuration Section' in the 'BaseCore/config.yml'. The next information may not be saved, Trying to save again by a flat file.");
		mc.addDefault("SQLProblem", "There's a problem with the SQL. Please check your config / contact to 'BaseCore' developer.");
		mc.addDefault("SerializableError", "The object has to implement from Serializable to use SerializableUtils.");
		mc.addDefault("CantSaveType", "The type is not supported to be saved on the saver. Please contact the BaseCore creator.");
		BaseCore.runFirst();
	}

	@Override
	public void onDisable() {
		ConfigurableCore.onStop();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {		
		if (cmd.equalsIgnoreCase("basecore")) 
			mc.send("BaseCore is a library that created by DrBenana.", sender);
		return false;
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
	public static boolean arrayContains(Object[] array, Object o) {
		if (array.length == 0) return true;
		for (Object ao : array)
			if (ao == o) return true;
		return false;
	}
	
	public static boolean arrayContains(double[] array, double o) {
		if (array.length == 0) return true;
		for (double ao : array)
			if (ao == o) return true;
		return false;
	}
	
	public static boolean stringArrayContains(String[] array, String o) {
		if (array.length == 0) return true;
		for (String ao : array)
			if (ao != null && ao.equals(o)) return true;
		return false;
	}
	
	public static boolean stringArrayContainsIgnoreCase(String[] array, String o) {
		if (array.length == 0) return true;
		for (String ao : array)
			if (ao != null && ao.equalsIgnoreCase(o)) return true;
		return false;
	}

	public static MessageCore getMessageCore() {
		return mc;
	}
}

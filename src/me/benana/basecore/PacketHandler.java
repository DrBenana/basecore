package me.benana.basecore;

import org.bukkit.entity.Player;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import me.benana.basecore.events.ReceivePacketEvent;
import me.benana.basecore.events.SendPacketEvent;

/** 
 * You don't have to touch it.
 * 
 * @author DrBenana
 */
public class PacketHandler extends ChannelDuplexHandler {
	private final Player p;
	
	/**
	 * @param player The player that the packet is sent to.
	 */
	public PacketHandler(Player player) {
		p = player;
	}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object packet) throws Exception {
		ReceivePacketEvent e = new ReceivePacketEvent(ctx, packet, getPlayer());
		if (!e.isCancelled()) super.channelRead(e.getChannelHandlerContext(), e.getPacket());
	}
	
	@Override
	public void write(ChannelHandlerContext ctx, Object packet, ChannelPromise promise) throws Exception {
		SendPacketEvent e = new SendPacketEvent(ctx, packet, promise, getPlayer());
		if (!e.isCancelled()) super.write(e.getChannelHandlerContext(), e.getPacket(), e.getChannelPromise());
	}
	
	/**
	 * @return The player that the packet is sent to.
	 */
	public Player getPlayer() {
		return p;
	}
	
}

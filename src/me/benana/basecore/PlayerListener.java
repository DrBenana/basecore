package me.benana.basecore;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import me.benana.basecore.events.EnableServerEvent;
import me.benana.basecore.events.SendPacketEvent;
import me.benana.basecore.utils.JavaUtils;
import me.benana.basecore.utils.PacketUtils;
import me.benana.basecore.utils.PlayerUtils;
import me.benana.basecore.utils.reflection.FieldUtils;

/**
 * Listener for players. You don't have really to touch it.
 * 
 * @author DrBenana
 */
public class PlayerListener implements Listener {	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {	
		PlayerUtils.getClonedRealNameMap().keySet().forEach(s ->
			e.setMessage(e.getMessage().replaceAll(s, Bukkit.getPlayer(PlayerUtils.getClonedRealNameMap().get(s)).getName())));
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		BaseCore.createPlayerPacketChannel(e.getPlayer());
	}
	
	@EventHandler
	public void onTabCommand(SendPacketEvent e) {
		if (PacketUtils.getPacketType(e.getPacket()).equals("PacketPlayOutTabComplete")) {
			if (!JavaUtils.containsAny(PlayerUtils.fakeNames, Arrays.asList((String[]) FieldUtils.getDeclaredField(e.getPacket(), "a")))) return;
			BaseCore.debug(e.getPacket());
			FieldUtils.setDeclaredField(e.getPacket(), "a", PlayerUtils.getClonedRealNameMap().keySet().toArray(new String[PlayerUtils.getClonedRealNameMap().keySet().size()]));
		}
	}
	
	@EventHandler
	public void onEnableServer(EnableServerEvent e) {
		Bukkit.getOnlinePlayers().forEach(p -> BaseCore.createPlayerPacketChannel(p));
	}
	
}

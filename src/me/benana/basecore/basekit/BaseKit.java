package me.benana.basecore.basekit;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.basecore.Main;

/**
 * BaseKit allows you to create "ShortEvents" for a specific player, so you don't have to check what player doing the event. The ShortEvent checks it with lot of more things that you can set in the annotation.
 * 
 * @author DrBenana
 */
public class BaseKit implements Listener, Serializable {
	private static final long serialVersionUID = 5552854460603921249L;
	private static Map<UUID, BaseKit> players = new HashMap<>();
	private transient Player p;
	private UUID uuid;
	
	/**
	 * @param e A player who will get the BaseKit.
	 */
	public BaseKit(Player e) {
		this(e, Main.getPlugin());
	}
	
	/**
	 * @param e A player who will get the BaseKit.
	 * @param pl The JavaPlugin that the listeners will be registered on.
	 */
	public BaseKit(Player e, JavaPlugin pl) {
		// Basic Information
		if (players.containsKey(e.getUniqueId())) removeBaseKit(e);
		players.put(e.getUniqueId(), this);
		this.p = e;
		this.uuid = e.getUniqueId();
				
		// Registering
		Bukkit.getPluginManager().registerEvents(this, pl);
		BaseKitCore.registerBaseKit(this);
	}
	
	/**
	 * @return The player of the BaseKit.
	 */
	public Player getPlayer() {
		if (p == null) p = Bukkit.getPlayer(uuid);
		return p;
	}
	
	public UUID getUUID() {
		return this.uuid;
	}
	
	/**
	 * @param p A player with a BaseKit.
	 * @return The BaseKit of the player.
	 */
	public static BaseKit getBaseKit(Player p) {
		if (!players.containsKey(p.getUniqueId())) return null;
		return players.get(p.getUniqueId());
	}
	
	public void removeBaseKit() {
		// Remove Basic Information
		players.remove(p.getUniqueId());
		p = null;
		
		// UnRegistering
		HandlerList.unregisterAll(this);
		BaseKitCore.unRegisterBaseKit(this);
	}
	
	/**
	 * @param p A player to remove from the BaseKit.
	 */
	public static void removeBaseKit(Player p) {
		players.get(p.getUniqueId()).removeBaseKit();
	}
}

package me.benana.basecore.basekit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import me.benana.basecore.Main;
import me.benana.basecore.exceptions.InvalidAnnotationException;
import me.benana.basecore.general.BodyPart;
import me.benana.basecore.utils.JavaUtils;

public class BaseKitCore implements Listener {
	private static List<BaseKit> basekits = new ArrayList<>();
	
	public static void registerBaseKit(BaseKit bk) {
		basekits.add(bk);
	}
	
	public static void unRegisterBaseKit(BaseKit bk) {
		basekits.remove(bk);
	}
	
	/*
	 * EVENTS!!
	 */
	
	// Interact
	@EventHandler
	public void onInteractEvent(PlayerInteractEvent e) {
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(e.getPlayer().getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(InteractEvent.class)) {
						InteractEvent an = m.getAnnotation(InteractEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rank
						if (e.getClickedBlock() != null && !Main.arrayContains(an.block_material(), e.getClickedBlock().getType())) continue; // if material of clicked_block
						if ((e.getPlayer().getKiller().getItemInHand() == null && !Main.arrayContains(an.hand_material(), Material.AIR)) || 
								!Main.arrayContains(an.hand_material(), e.getPlayer().getKiller().getItemInHand().getType())) continue; // if material of item_int_hand
						if (e.getPlayer().getItemInHand() != null && !Main.stringArrayContains(an.hand_name(), e.getPlayer().getItemInHand().getItemMeta().getDisplayName())) continue; // if name of item_in_hand
						if (!Main.arrayContains(an.action(), e.getAction())) continue; // if action
						if (!an.permission().equals("") && !e.getPlayer().hasPermission(an.permission())) continue; // if permission
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// RightClickEntityEvent
	@EventHandler
	public void onRightInteractEntityEvent(PlayerInteractAtEntityEvent e) {
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(e.getPlayer().getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(RightClickEntityEvent.class)) {
						RightClickEntityEvent an = m.getAnnotation(RightClickEntityEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rand
						if ((e.getPlayer().getKiller().getItemInHand() == null && !Main.arrayContains(an.hand_material(), Material.AIR)) || 
								!Main.arrayContains(an.hand_material(), e.getPlayer().getKiller().getItemInHand().getType())) continue; // if material of item_int_hand
						if (e.getPlayer().getItemInHand() != null && !Main.stringArrayContains(an.hand_name(), e.getPlayer().getItemInHand().getItemMeta().getDisplayName())) continue; // if name of item_in_hand
						if (!Main.arrayContains(an.entity(), e.getRightClicked().getType())) continue; // if entity type
						if (!an.permission().equals("") && !e.getPlayer().hasPermission(an.permission())) continue; // if permission
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// DamageEntityEvent
	@EventHandler
	public void onLeftInteractEntityEvent(EntityDamageByEntityEvent e) {
		if (!(e.getDamager() instanceof Player)) return;
		Player p = (Player) e.getDamager();
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(p.getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(DamageEntityEvent.class)) {
						DamageEntityEvent an = m.getAnnotation(DamageEntityEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rand
						if ((p.getItemInHand() == null && !Main.arrayContains(an.hand_material(), Material.AIR)) || 
								!Main.arrayContains(an.hand_material(), p.getItemInHand().getType())) continue; // if material of item_in_hand
						if (p.getItemInHand() != null && !Main.stringArrayContains(an.hand_name(), p.getItemInHand().getItemMeta().getDisplayName())) continue; // if name of item_in_hand
						if (!Main.arrayContains(an.entity(), p.getType())) continue; // if entity type
						if (!Main.stringArrayContains(an.entity_uuid(), e.getEntity().getUniqueId().toString()))
						if (!an.permission().equals("") && !p.hasPermission(an.permission())) continue; // if permission
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// DeathEvent
	@EventHandler
	public void onDeathEvent(PlayerDeathEvent e) {
		if (!(e.getEntity() instanceof Player)) return;
		Player p = (Player) e.getEntity();
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(p.getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(DeathEvent.class)) {
						DeathEvent an = m.getAnnotation(DeathEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rand
						if (!an.permission().equals("") && !p.hasPermission(an.permission())) continue; // if permission
						if (!Main.arrayContains(an.cause(), p.getLastDamageCause().getCause())) // if cause
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// KillEvent
	@EventHandler
	public void onKillEvent(EntityDeathEvent e) {
		if (e.getEntity().getKiller() == null) return;
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(e.getEntity().getKiller().getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(KillEvent.class)) {
						KillEvent an = m.getAnnotation(KillEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rank
						if (!an.permission().equals("") && !e.getEntity().getKiller().hasPermission(an.permission())) continue; // if permission
						if (!Main.arrayContains(an.entity(), e.getEntity().getType())) continue; // if entity type
						if ((e.getEntity().getKiller().getItemInHand() == null && !Main.arrayContains(an.hand_material(), Material.AIR)) || 
								!Main.arrayContains(an.hand_material(), e.getEntity().getKiller().getItemInHand().getType())) continue; // if material of item_in_hand
						if (e.getEntity().getKiller().getItemInHand() != null && !Main.stringArrayContains(an.hand_name(), e.getEntity().getKiller().getItemInHand().getItemMeta().getDisplayName())) continue; // if name of item_in_hand
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// LaunchProjectileEvent
	@EventHandler
	public void onLaunchProjectileEvent(ProjectileLaunchEvent e) {
		if (!(e.getEntity().getShooter() instanceof Player)) return;
		Player p = (Player) e.getEntity().getShooter();
		
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(p.getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(LaunchProjectileEvent.class)) {
						LaunchProjectileEvent an = m.getAnnotation(LaunchProjectileEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rand
						if (!an.permission().equals("") && !p.hasPermission(an.permission())) continue; // if permission
						if (!Main.arrayContains(an.projectile_type(), e.getEntity().getType())) continue; // if type of entity
						if (!Main.stringArrayContains(an.projectile_uuid(), e.getEntity().getUniqueId().toString())) continue; // if uuid of entity
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// HitProjectileEvent
	@EventHandler
	public void onHitProjectileEvent(ProjectileHitEvent e) {
		if (!(e.getEntity().getShooter() instanceof Player)) return;
		Player p = (Player) e.getEntity().getShooter();
		
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(p.getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(HitProjectileEvent.class)) {
						HitProjectileEvent an = m.getAnnotation(HitProjectileEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rand
						if (!an.permission().equals("") && !p.hasPermission(an.permission())) continue; // if permission
						if (!Main.arrayContains(an.projectile_type(), e.getEntity().getType())) continue; // if type of entity
						if (!Main.stringArrayContains(an.projectile_uuid(), e.getEntity().getUniqueId().toString())) continue; // if uuid of entity
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// DamageProjectileEvent
	@EventHandler
	public void onDamageProjectileEvent(EntityDamageByEntityEvent e) {
		if (!(e.getDamager() instanceof Projectile)) return;
		Projectile proj = (Projectile) e.getDamager();
		if (!(proj.getShooter() instanceof Player)) return;
		Player p = (Player) proj.getShooter();
		
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(p.getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(DamageProjectileEvent.class)) {
						DamageProjectileEvent an = m.getAnnotation(DamageProjectileEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rand
						if (!an.permission().equals("") && !p.hasPermission(an.permission())) continue; // if permission
						if (!Main.arrayContains(an.projectile_type(), proj.getType())) continue; // if type of proj
						if (!Main.stringArrayContains(an.projectile_uuid(), proj.getUniqueId().toString())) continue; // if uuid of proj
						if (!Main.arrayContains(an.target_type(), e.getEntity().getType())) continue; // if type of target
						if (an.min_damage() != 404 && an.min_damage() > e.getDamage()) continue; // if min damage {It's negative because of the continue)
						if (an.max_damage() != 404 && an.max_damage() < e.getDamage()) continue; // if max damage
						if (!Main.arrayContains(an.damage(), e.getDamage())) continue; // if exact damage
						if (!Main.arrayContains(an.part(), BodyPart.touchAt(p.getLocation(), proj.getLocation()))) continue; // if body part
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
	
	// DamageByEntityEvent
	@EventHandler
	public void onDamageByEntityEvent(EntityDamageByEntityEvent e) {
		if (!(e.getEntity() instanceof Player)) return;
		Player p = (Player) e.getEntity();
		
		for (BaseKit b : basekits) {
			if (b.getPlayer().getUniqueId().toString().equals(p.getUniqueId().toString())) {
				for (Method m : b.getClass().getMethods()) {
					if (m.isAnnotationPresent(DamageByEntityEvent.class)) {
						DamageByEntityEvent an = m.getAnnotation(DamageByEntityEvent.class);
						if (an.rand().length > 1 && !JavaUtils.chance(an.rand()[0], an.rand()[1])) continue; // if rand
						if (!an.permission().equals("") && !p.hasPermission(an.permission())) continue; // if permission
						if (!Main.arrayContains(an.entity(), e.getDamager().getType())) continue; // if type of proj
						if (!Main.stringArrayContains(an.entity_uuid(), e.getDamager().getUniqueId().toString())) continue; // if uuid of proj
						if (an.min_damage() != 404 && an.min_damage() > e.getDamage()) continue; // if min damage {It's negative because of the continue)
						if (an.max_damage() != 404 && an.max_damage() < e.getDamage()) continue; // if max damage
						if (!Main.arrayContains(an.damage(), e.getDamage())) continue; // if exact damage
						try {
							m.invoke(b, e);
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
							throw new InvalidAnnotationException("The argument of the method is not compatible with the annotation.");
						}
					}
				}
			}
		}
	}
}

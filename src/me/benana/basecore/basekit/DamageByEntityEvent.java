package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.bukkit.entity.EntityType;

/**
 * <b> ShortEvent </b> that runs when the player gets damage by another entity. 
 * 
 * @author DrBenana
 * @parameter EntityDamageByEntityEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface DamageByEntityEvent {
	// Default
	public int[] rand() default {};
	public String permission() default "";
	
	// Specific
	public EntityType[] entity() default {};
	public String[] entity_uuid() default {};
	public double min_damage() default 404;
	public double max_damage() default 404;
	public double[] damage() default {};
}

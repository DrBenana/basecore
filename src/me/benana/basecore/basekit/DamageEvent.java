package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <b> ShortEvent </b> that runs when the player gets damage.
 * 
 * @author DrBenana
 * @parameter PlayerDamageEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface DamageEvent {

}

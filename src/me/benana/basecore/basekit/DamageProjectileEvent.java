package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.bukkit.entity.EntityType;

import me.benana.basecore.general.BodyPart;

/**
 * <b> ShortEvent </b> that runs when an arrow that the player has shot damages another entity.
 * 
 * @author DrBenana
 * @parameter EntityDamageByEntityEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface DamageProjectileEvent {
	// Default
	public int[] rand() default {};
	public String permission() default "";
	
	// Specific
	public String[] projectile_uuid() default {};
	public EntityType[] projectile_type() default {};
	public EntityType[] target_type() default {};
	public double min_damage() default 404;
	public double max_damage() default 404;
	public BodyPart[] part() default {};
	public double[] damage() default {};
}

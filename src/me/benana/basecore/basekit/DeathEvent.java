package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

/**
 * <b> ShortEvent </b> that runs when the player deaths. 
 * 
 * @author DrBenana
 * @parameter PlayerDeathEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface DeathEvent {
	// Default
	public int[] rand() default {};
	public String permission() default "";
	
	// Specific
	public DamageCause[] cause() default {}; 
}

package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.bukkit.entity.EntityType;

/**
 * <b> ShortEvent </b> that runs when an arrow that the player has shot hits something.
 * 
 * @author DrBenana
 * @parameter ProjectileHitEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface HitProjectileEvent {
	// Default
	public int[] rand() default {};
	public String permission() default "";
	
	// Specific
	public String[] projectile_uuid() default {};
	public EntityType[] projectile_type() default {};
}

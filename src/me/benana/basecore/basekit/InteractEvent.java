package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.bukkit.Material;
import org.bukkit.event.block.Action;

/**
 * <b> ShortEvent </b> that runs when the player interacts. 
 * 
 * @author DrBenana
 * @parameter PlayerInteractEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface InteractEvent {
	// Default
	public int[] rand() default {};
	public String permission() default "";
	
	// Specific
	public Action[] action() default {};
	public Material[] block_material() default {};
	public Material[] hand_material() default {};
	public String[] hand_name() default {};
}

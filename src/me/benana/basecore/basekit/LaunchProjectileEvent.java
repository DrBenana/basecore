package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.bukkit.entity.EntityType;

/**
 * <b> ShortEvent </b> that runs when the player shoots a projectile. 
 * 
 * @author DrBenana
 * @parameter ProjectileLaunchEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface LaunchProjectileEvent {
	// Default
	public int[] rand() default {};
	public String permission() default "";
	
	// Specific
	public String[] projectile_uuid() default {};
	public EntityType[] projectile_type() default {};
}

package me.benana.basecore.basekit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

/**
 * <b> ShortEvent </b> that runs when the player right clicks another entity.
 * 
 * @author DrBenana
 * @parameter PlayerInteractEntityEvent
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface RightClickEntityEvent {
	// Default
	public int[] rand() default {};
	public String permission() default "";
	
	// Specific
	public Material[] hand_material() default {};
	public EntityType[] entity() default {};
	public String[] hand_name() default {};
}

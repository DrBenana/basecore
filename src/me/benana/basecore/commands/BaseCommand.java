package me.benana.basecore.commands;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface BaseCommand {
	String[] label();
	String description();
	String[] arguments() default {};
	String defaultpermission() default "";
	String usage() default "";
	String noPermissionMessage() default "�cYou don't have enough permissions for this command";
	String invalidArgumentsMesssage() default "�cInvalid Arguments.";
}

package me.benana.basecore.commands;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

import me.benana.basecore.general.Parameter;
import me.benana.basecore.utils.JavaUtils;
import me.benana.basecore.utils.reflection.ClassUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

public class CommandCore {
	
	public static void registerCommandCore(Object... objects) {
		for (Object o : objects) {
			for (Method m : o.getClass().getMethods()) {
				if (m.isAnnotationPresent(BaseCommand.class)) {
					BaseCommand c = m.getAnnotation(BaseCommand.class);
					BukkitCommand cmd = new BukkitCommand(c.label()[0]) {
						@SuppressWarnings("deprecation")
						@Override
						public boolean execute(CommandSender sender, String cmd, String[] arguments) {
							for (int i = 0; i < c.arguments().length; i++) {
								String perm = c.arguments()[i].contains(":") ? c.arguments()[i].split(":")[1] : "";
								String args = c.arguments()[i].split(":")[0];
								if (!(sender.hasPermission(perm) || perm.equals(""))) {
									sender.sendMessage(c.noPermissionMessage());
									return true;
								}
								String[] sargs = args.split(" ");
								for (int f = 0; f < sargs.length; f++) {
									if (sargs[f].equals("**")) break;
									if (sargs[f].equals("*")) continue;
									if (arguments.length > f) {
										if (sargs[f].equals("{on_p}") && Bukkit.getPlayerExact(arguments[f]) != null) continue;
										if (sargs[f].equals("{p}") && (Bukkit.getOfflinePlayer(arguments[f]).hasPlayedBefore() || Bukkit.getPlayerExact(arguments[f]) != null)) continue;
										if (sargs[f].equals("{num}") && JavaUtils.isInteger(arguments[f])) continue;
										if (sargs[f].equals(arguments[f])) continue;
									} else { break; }
									sender.sendMessage(c.invalidArgumentsMesssage());
									return true;
								}
							}
							ClassUtils.invokeDeclaredMethod(o, m.getName(), 
									new Parameter(CommandSender.class, sender),
									new Parameter(String.class, cmd),
									new Parameter(String[].class, arguments));
							return true;
						}
					};
					
					ArrayList<String> aliases = new ArrayList<>();
					for (int i = 1; i < c.label().length; i++) aliases.add(c.label()[i]);
					cmd.setAliases(aliases);
					cmd.setDescription(c.description());
					if (!c.usage().equals("")) cmd.setUsage(c.usage());
					NMSUtils.getCommandMap().register(c.label()[0], cmd);
				}
			}
		}
	}

}

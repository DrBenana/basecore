package me.benana.basecore.economy;

import java.io.Serializable;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import me.benana.basecore.BaseCore;
import me.benana.basecore.Main;
import me.benana.basecore.information.Saver;

/**
 * The Economy system of "BaseCore" allows you to add, remove and set the money of some player. Note: The system can interact with vault if the server's owner sets it on the BaseCore's config.
 * Also, You don't have to check if vault don't exist. BaseCore does it automatically and if there's no Vault, BaseCore uses his one system.
 * 
 * @author DrBenana
 */
public class Economy implements Serializable {
	private static final long serialVersionUID = 1752436673097757486L;
	protected static Saver saver = BaseCore.getDefaultSaver("economy", Main.getPlugin());
	private UUID uuid;
	private transient OfflinePlayer p;
	
	/**
	 * @param p An OfflinePlayer.
	 */
	public Economy(OfflinePlayer p) {
		this.p = p;
		this.uuid = p.getUniqueId();
	}
	
	/**
	 * @return Balance of the player.
	 */
	public double getBalance() {
		return getBalance(p);
	}
	
	/**
	 * @param i The balance to set to the player.
	 */
	public Economy setBalance(double i) {
		setBalance(p, i);
		return this;
	}
	
	/**
	 * @param i The balance to add to the player.
	 */
	public Economy addBalance(double i) {
		addBalance(p, i);
		return this;
	}
	
	/**
	 * @param i The balance to remove from the player.
	 */
	public Economy delBalance(double i) {
		delBalance(p, i);
		return this;
	}
	
	/**
	 * @return OfflinePlayer of the economy
	 */
	public OfflinePlayer getPlayer() {
		if (this.p == null) this.p = Bukkit.getOfflinePlayer(uuid);
		return this.p;
	}
	
	/**
	 * @param p An OfflinePlayer.
	 * @return Balance of the player.
	 */
	public static double getBalance(OfflinePlayer p) {
		if (VaultEconomy.useVault()) return VaultEconomy.getVaultEconomy().getBalance(p);
		return saver.getDouble(p.getUniqueId().toString());
	}
	
	/**
	 * @param p An OfflinePlayer.
	 * @param i The balance to set to the player.
	 */
	public static void setBalance(OfflinePlayer p, double i) {
		if (VaultEconomy.useVault()) {
			VaultEconomy.getVaultEconomy().withdrawPlayer(p, getBalance(p));
			VaultEconomy.getVaultEconomy().depositPlayer(p, i);
		}
		saver.set(p.getUniqueId().toString(), i);
	}
	
	/**
	 * @param p An OfflinePlayer.
	 * @param i The balance to add to the player.
	 */
	public static void addBalance(OfflinePlayer p, double i) {
		setBalance(p, getBalance(p) + i);
	}
	
	/**
	 * @param p An OfflinePlayer.
	 * @param i The balance to remove from the player.
	 */
	public static void delBalance(OfflinePlayer p, double i) {
		setBalance(p, getBalance(p) - i);
	}

}

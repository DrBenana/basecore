package me.benana.basecore.economy;

import org.bukkit.plugin.RegisteredServiceProvider;

import me.benana.basecore.Main;
import net.milkbowl.vault.economy.Economy;

public class VaultEconomy {
	private static Economy econ;
	private static boolean useVault;
	
	/**
	 * Trying to setup the VaultEconomy at the plugin. You don't have to use it.
	 * 
	 * @return
	 */
	public static boolean setupEconomy() {
        if (Main.getPlugin().getServer().getPluginManager().getPlugin("Vault") == null) {
        	useVault = false;
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = Main.getPlugin().getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
        	useVault = false;
            return false;
        }
        econ = rsp.getProvider();
        useVault = ((econ != null) == Main.getPlugin().getConfig().getBoolean("Economy.useVault"));
        return useVault;
    }

	/**
	 * @return Economy of Vault.
	 */
	public static Economy getVaultEconomy() {
		return econ;
	}
	
	/**
	 * @return If the server has vault.
	 */
	public static boolean useVault() {
		return useVault;
	}
	
}

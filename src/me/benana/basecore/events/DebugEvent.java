package me.benana.basecore.events;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class DebugEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private final String message;
    private final CommandSender[] senders;
    private boolean cancelled = false;
    
    public DebugEvent(String msg, CommandSender[] senders) {
		this.message = msg;
		this.senders = senders;
		Bukkit.getPluginManager().callEvent(this);
	}
    
    public String getDebugMessage() {
    	return this.message;
    }
    
    public CommandSender[] getGetters() {
    	return this.senders;
    }
    
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		cancelled = cancel;
		
	}

}

package me.benana.basecore.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Event (of a Bukkit listener) that runs when the server enables, after all of the plugins.
 * 
 * @author DrBenana
 */
public class EnableServerEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}

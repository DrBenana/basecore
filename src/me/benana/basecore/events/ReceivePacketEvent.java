package me.benana.basecore.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import io.netty.channel.ChannelHandlerContext;

/**
 * Event that is called when a packet is received from a player.
 * 
 * @author DrBenana
 */
public class ReceivePacketEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private ChannelHandlerContext ctx;
    private Object packet;
    private boolean cancelled = false;
    private final Player player;
    
    /**
     * @param ctx A ChannelHandlerContext
     * @param packet A Packet
     * @param player The player that the packet is received from.
     */
    public ReceivePacketEvent(ChannelHandlerContext ctx, Object packet, Player player) {
		this.ctx = ctx;
		this.packet = packet;
		this.player = player;
		Bukkit.getPluginManager().callEvent(this);
	}
    
	/**
	 * @return The ChannelHandlerContext
	 */
    public ChannelHandlerContext getChannelHandlerContext() {
    	return ctx;
    }
    
	/**
	 * @return The player that the packet is received from.
	 */
    public Player getPlayer() {
    	return player;
    }
    
	/**
	 * @return The packet that is received.
	 */
    public Object getPacket() {
    	return packet;
    }
    
	/**
	 * @param packet The packet to set.
	 */
    public void setPacket(Object packet) {
    	this.packet = packet;
    }
    
	/**
	 * @return Is the event cancelled.
	 */
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * @param paramBoolean Cancel the event.
	 */
	@Override
	public void setCancelled(boolean paramBoolean) {
		cancelled = paramBoolean;
	}
    
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

}

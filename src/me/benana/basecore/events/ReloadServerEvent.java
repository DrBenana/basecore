package me.benana.basecore.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Event (of a Bukkit listener) that runs immediately when a reload command is done.
 * 
 * @author DrBenana
 */
public class ReloadServerEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}

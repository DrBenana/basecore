package me.benana.basecore.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

/**
 * Event that is called when the server tries to send a packet to the player.
 * 
 * @author DrBenana
 */
public class SendPacketEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private ChannelHandlerContext ctx;
	private Object packet;
	private ChannelPromise promise;
	private boolean cancelled = false;
	private final Player player;

	/**
	 * @param ctx A ChannelHandlerContext
	 * @param packet A Packet
	 * @param promise A ChannelPromise
	 * @param player The player that the packet will be sent to.
	 */
	public SendPacketEvent(ChannelHandlerContext ctx, Object packet, ChannelPromise promise, Player player) {
		this.ctx = ctx;
		this.packet = packet;
		this.promise = promise;
		this.player = player;
		Bukkit.getPluginManager().callEvent(this);
	}

	/**
	 * @return The ChannelHandlerContext
	 */
	public ChannelHandlerContext getChannelHandlerContext() {
		return ctx;
	}

	/**
	 * @return The player that the packet will be sent to.
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @return The packet that will be sent to the player.
	 */
	public Object getPacket() {
		return packet;
	}

	/**
	 * @return The ChannelPromise
	 */
	public ChannelPromise getChannelPromise() {
		return promise;
	}

	/**
	 * @param packet The packet to set.
	 */
	public void setPacket(Object packet) {
		this.packet = packet;
	}

	/**
	 * @return Is the event cancelled.
	 */
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * @param paramBoolean Cancel the event.
	 */
	@Override
	public void setCancelled(boolean paramBoolean) {
		cancelled = paramBoolean;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}

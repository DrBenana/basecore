package me.benana.basecore.exceptions;

public class DefaultNotExistException extends GeneralBaseCoreException {
	private static final long serialVersionUID = 3564352229322314777L;
	
	public DefaultNotExistException(String msg) {
		super(msg);
	}

}

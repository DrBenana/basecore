package me.benana.basecore.exceptions;

public class GeneralBaseCoreException extends RuntimeException {
	private static final long serialVersionUID = -4693386448008082565L;

	public GeneralBaseCoreException(String msg) {
		super(msg);
	}
	
	public GeneralBaseCoreException(Throwable t) {
		super(t);
	}

}

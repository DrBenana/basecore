package me.benana.basecore.exceptions;

public class InvalidAnnotationException extends GeneralBaseCoreException {
	private static final long serialVersionUID = 4593343428833202162L;
	
	public InvalidAnnotationException(String msg) {
		super(msg);
	}
	
}

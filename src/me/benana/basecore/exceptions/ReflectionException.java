package me.benana.basecore.exceptions;

public class ReflectionException extends GeneralBaseCoreException {
	private static final long serialVersionUID = -4370749357443051057L;

	public ReflectionException(Throwable t) {
		super(t);
	}

}

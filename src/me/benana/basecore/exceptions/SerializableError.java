package me.benana.basecore.exceptions;

public class SerializableError extends GeneralBaseCoreException {
	private static final long serialVersionUID = 5397140726150796375L;

	public SerializableError(String msg) {
		super(msg);
	}
}

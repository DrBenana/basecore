package me.benana.basecore.general;

import org.bukkit.Location;

/**
 * BodyPart is a system that can check what is the body part of the location on the player. Very useful with projectiles.
 * 
 * @author DrBenana
 */
public enum BodyPart {
	Head, Body;
	
	/**
	 * @param player The location of the target player
	 * @param proj The location of the projectile
	 * @return The part of the body that the projectile touches him.
	 */
	public static BodyPart touchAt(Location player, Location proj) {
		if (proj.getY() - player.getY() > 1.30d) return BodyPart.Head;
		else return BodyPart.Body;
	}
}

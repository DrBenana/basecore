package me.benana.basecore.general;

import java.io.Serializable;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.benana.basecore.exceptions.DefaultNotExistException;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * A really useful system that allows you to add a default prefix / suffix to your messages. Also it allows you to add default messages to specific situations.
 * TODO A really big update.
 * 
 * @author DrBenana
 */
public class MessageCore implements Serializable {
	private static final long serialVersionUID = -3978281532082982951L;
	private String prefix, suffix;
	private HashMap<String, String> defaults = new HashMap<>();
	
	/**
	 * @param p Prefix.
	 * @param s Suffix.
	 */
	public MessageCore(String p, String s) {
		this.prefix = p; this.suffix = s;
	}
	
	/**
	 * @param p Prefix.
	 */
	public MessageCore(String p) {
		this(p, "");
	}
	
	public MessageCore() {
		this("");
	}
	
	/**
	 * @param key The name of the default.
	 * @param value The value of the default.
	 */
	public void addDefault(String key, TextComponent value) {
		defaults.put(key, value.getText());
	}
	
	/**
	 * @param key The name of the default.
	 * @param value The value of the default.
	 */
	public void addDefault(String key, String value) {
		defaults.put(key, value);
	}
	
	/**
	 * @param key The name of the default.
	 * @return Is key exist.
	 */
	public boolean removeDefault(String key) {
		if (!defaults.containsKey(key))
			throw new DefaultNotExistException("The default is not exist. You have to add the default before you try to send it.");
		defaults.remove(key);
		return true;
	}
	
	/**
	 * @param msg String message to send.
	 */
	public void send(String msg) {
		Bukkit.broadcastMessage(prefix + msg + suffix);
	}

	/**
	 * @param msg TextComponent message to send.
	 * @param to CommandSender/s to get the message.
	 */
	public void send(TextComponent msg, Player... to) {
		for (Player t : to) {
			TextComponent txt = new TextComponent(prefix);
			txt.addExtra(msg);
			txt.addExtra(suffix);
			t.spigot().sendMessage(txt);
		}
	}
	
	/**
	 * @param msg String message to send.
	 * @param to CommandSender/s to get the message.
	 */
	public void send(String msg, CommandSender... to) {
		for (CommandSender t : to)
			t.sendMessage(prefix + msg + suffix);
	}
	
	/**
	 * @param msg TextComponent message to send.
	 * @param permission The permission of the players who get the message.
	 */
	public void send(TextComponent msg, String permission) {
		for (Player p : Bukkit.getOnlinePlayers()) if (p.hasPermission(permission)) this.send(msg, p);
	}
	
	/**
	 * @param msg String message to send.
	 * @param permission The permission of the players who get the message.
	 */
	public void send(String msg, String permission) {
		for (Player p : Bukkit.getOnlinePlayers()) if (p.hasPermission(permission)) this.send(msg, p);
	}
	
	/**
	 * @param def The name of the default.
	 * @param to CommandSender/s to get the message.
	 * @return Is key exist.
	 */
	public boolean sendDefault(String def, CommandSender... to) {
		if (!defaults.containsKey(def))
			throw new DefaultNotExistException("The default is not exist. You have to add the default before you try to send it.");
		this.send(defaults.get(def), to);
		return true;
	}
	
	/**
	 * @param def The name of the default.
	 * @param permission The permission of the players who get the message.
	 * @return Is key exist.
	 */
	public boolean sendDefault(String def, String permission) {
		if (!defaults.containsKey(def))
			throw new DefaultNotExistException("The default is not exist. You have to add the default before you try to send it.");
		this.send(defaults.get(def), permission);
		return true;
	}
	
	
	/**
	 * @param def The name of the default to broadcast.
	 * @return Is default exist.
	 */
	public boolean sendDefault(String def) {
		if (!defaults.containsKey(def))
			throw new DefaultNotExistException("The default is not exist. You have to add the default before you try to send it.");
		Bukkit.broadcastMessage(prefix + defaults.get(def) + suffix);
		return true;
	}
	
	/**
	 * @return Map<String, TextComponent> of all of the defaults.
	 */
	public HashMap<String, String> getDefaults(){ 
		return defaults;
	}
	
	/**
	 * @param s The original string
	 * @param args The arguments
	 * @return Formatted String
	 */
	public static String formatter(String s, String... args) {
		for (int i = 0; i < args.length; i++) {
			s = s.replaceFirst("{}", args[i]);
			s = s.replaceAll("{" + i + "}", args[i]);
		}
		return s;
	}
}

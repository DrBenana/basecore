package me.benana.basecore.general;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import me.benana.basecore.utils.JavaUtils;

/**
 * Region allows you to manage a region that creates between 2 locations. 
 * 
 * @author DrBenana
 */
public class Region implements Serializable {
	private static final long serialVersionUID = 7822411419453342305L;
	private double[] x = new double[2], y = new double[2], z = new double[2];
	private String worldname;
	private transient World world;
	
	/**
	 * @param world World of the region.
	 * @param x0 X cord of the first block in the region.
	 * @param y0 Y cord of the first block in the region.
	 * @param z0 Z cord of the first block in the region.
	 * @param x0 X cord of the last (second) block in the region.
	 * @param y0 Y cord of the last (second) block in the region.
	 * @param z0 Z cord of the last (second) block in the region.
	 */
	public Region(World world, double x0, double y0, double z0, double x1, double y1, double z1) {
		this.x[0] = x0;
		this.x[1] = x1;
		this.y[0] = y0;
		this.y[1] = y1;
		this.z[0] = z0;
		this.z[1] = z1;
		this.world = world;
		this.worldname = world.getName();
	}
	
	/**
	 * @param world World of the region
	 * @param x0 X cord of the first block in the region.
	 * @param y0 Y cord of the first block in the region.
	 * @param z0 Z cord of the first block in the region.
	 * @param x0 X cord of the last (second) block in the region.
	 * @param y0 Y cord of the last (second) block in the region.
	 * @param z0 Z cord of the last (second) block in the region.
	 */
	public Region(World world, int x0, int y0, int z0, int x1, int y1, int z1) {
		this(world, (double) x0, (double) y0, (double) z0, (double) x1, (double) y1, (double) z1);
	}
	
	/**
	 * @param loc1 Location of the first block in the region.
	 * @param loc2 Location of the last (second) block in the region.
	 */
	public Region(Location loc1, Location loc2) {
		this(loc1.getWorld(), loc1.getX(), loc1.getY(), loc1.getZ(), loc2.getX(), loc2.getY(), loc2.getZ());
	}
	
	/**
	 * It's just copy a Region. 
	 * @param Region
	 */
	public Region(Region sa) {
		this(sa.getFirstLocation(), sa.getSecondLocation());
	}

	/**
	 * Adds x,y,z to the region. 
	 * @param x0 X to add to the first location.
	 * @param y0 Y to add to the first location.
	 * @param z0 Z to add to the first location.
	 * @param x1 X to add to the last location.
	 * @param y1 Y to add to the last location.
	 * @param z1 Z to add to the last location.
	 */
	public void add(double x0, double y0, double z0, double x1, double y1, double z1) {
		this.setFirstLocation(this.getFirstLocation().add(x0, y0, z0));
		this.setSecondLocation(this.getSecondLocation().add(x1, y1, z1));
	}
	
	/**
	 * Adds the same x,y,z to the region.
	 * @param x X to add.
	 * @param y Y to add.
	 * @param z Z to add.
	 */
	public void add(double x, double y, double z) {
		add(x, y, z, x, y, z);
	}
	
	/**
	 * @return The location of the center of the region.
	 */
	public Location getCenter() {
		return new Location(getWorld(), (x[0]+x[1])/2, (y[0]+y[1])/2, (z[0]+z[1])/2);
	}
	
	/**
	 * Sets all the blocks in the region to the selected material.
	 * @param m Material to set the blocks to.
	 */
	public void setBlocks(Material m) {
		for (Location loc : this.getAllLocations()) {
			loc.getBlock().setType(m);
		}
	}
	
	/**
	 * @return Location of the first block in the region.
	 */
	public Location getFirstLocation() {
		return new Location(getWorld(), this.x[0], this.y[0], this.z[0]);
	}
	
	/**
	 * @return Location of the last (second) block in the region.
	 */
	public Location getSecondLocation() {
		return new Location(getWorld(), this.x[1], this.y[1], this.z[1]);
	}
	
	/**
	 * @param Location of the first block in the region.
	 */
	public void setFirstLocation(Location loc) {
		this.x[0] = loc.getX();
		this.y[0] = loc.getY();
		this.z[0] = loc.getZ();
		this.world = loc.getWorld();
	}
	
	/**
	 * @param Location of the last (second) block in the region.
	 */
	public void setSecondLocation(Location loc) {
		this.x[1] = loc.getX();
		this.y[1] = loc.getY();
		this.z[1] = loc.getZ();
	}
	
	/**
	 * Checks if the location is found in the region.
	 * @param loc The location
	 * @return True when the location is found in the region and False when the location is not found in the region.
	 */
	public boolean contains(Location loc) {
		if (!loc.getWorld().getName().equals(this.getWorld().getName())) return false;
		if (JavaUtils.containsNumber(this.x[0], this.x[1], loc.getX()) &&
				JavaUtils.containsNumber(this.y[0], this.y[1], loc.getY()) &&
				JavaUtils.containsNumber(this.z[0], this.z[1], loc.getZ())) 
			return true;
		
		return false;
	} 
	
	/**
	 * @return List of the locations in the region (Includes air)
	 */
	public List<Location> getAllLocations() {
		List<Location> loc = new ArrayList<>();
		int x1 = (int) (this.x[0]<this.x[1] ? this.x[0] : this.x[1]);
		int x2 = (int) (this.x[0]>this.x[1] ? this.x[0] : this.x[1]);
		int y1 = (int) (this.y[0]<this.y[1] ? this.y[0] : this.y[1]);
		int y2 = (int) (this.y[0]>this.y[1] ? this.y[0] : this.y[1]);
		int z1 = (int) (this.z[0]<this.z[1] ? this.z[0] : this.z[1]);
		int z2 = (int) (this.z[0]>this.z[1] ? this.z[0] : this.z[1]);
		for(int x = x1; x <= x2; x++)
        	for(int z = z1; z <= z2; z++)
        		for(int y = y1; y <= y2; y++)
        			loc.add(new Location(getWorld(), x, y, z));
		return loc;
	}
	
	/**
	 * @return The world of the region.
	 */
	public World getWorld() {
		if (world == null) world = Bukkit.getWorld(worldname);
		return world;
	}
	
	public void paste(Location from, Location to) {
		for (Location loc : getAllLocations()) {
			Block b = loc.getBlock();
			loc = loc.add(to.getBlockX()-from.getBlockX(), to.getBlockY()-from.getBlockY(), to.getBlockZ()-from.getBlockZ());
			loc.getBlock().setType(b.getType());
			loc.getBlock().getState().setData(b.getState().getData());
		}
	}
	
	/**
	 * @return Amount of all the blocks in the region (Includes air)
	 */
	public long countBlocks() {
		int x = ((int) (this.x[0]<this.x[1] ? this.x[1]-this.x[0] : this.x[0]-this.x[1])) + 1,
		y = ((int) (this.y[0]<this.y[1] ? this.y[1]-this.y[0] : this.y[0]-this.y[1])) + 1,
		z = ((int) (this.z[0]<this.z[1] ? this.z[1]-this.z[0] : this.z[0]-this.z[1])) + 1;
		return x*y*z;
	}

}

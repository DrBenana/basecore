package me.benana.basecore.general;

/**
 * DDD
 * 
 * @author DrBenana
 */
public enum WorldType {
	NORMAL(0), NETHER(-1), END(1);
	
	private final int world;
	
	WorldType(int i) {
		world = i;
	}

	/**
	 * @return The id of the type
	 */
	public int getID() {
		return world;
	}
}

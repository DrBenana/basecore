package me.benana.basecore.information;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.List;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;

import me.benana.basecore.exceptions.GeneralBaseCoreException;
import me.benana.basecore.exceptions.SerializableError;
import me.benana.basecore.utils.SerializationUitls;

/**
 * ConfigSaver allows you easily to create custom configs. I recommend to use it only when you want to let the server's owner to change the values in the config. NOT SERIALIZABLE
 * 
 * @author Unknown.
 */
public class ConfigSaver extends YamlConfiguration implements Saver {
	private final File file;
	private final String defaults;
	private final JavaPlugin plugin;

	/**
	 * Creates new PluginFile, without defaults
	 * @param plugin - Your plugin
	 * @param fileName - Name of the file
	 */
	public ConfigSaver(JavaPlugin plugin, String fileName) {
		this(plugin, fileName, null);
	}

	/**
	 * Creates new PluginFile, with defaults
	 * @param plugin - Your plugin
	 * @param fileName - Name of the file
	 * @param defaultsName - Name of the defaults
	 */
	public ConfigSaver(JavaPlugin plugin, String fileName, String defaultsName) {
		if (!fileName.endsWith(".yml")) fileName = fileName + ".yml";
		this.plugin = plugin;
		this.defaults = defaultsName;
		this.file = new File(plugin.getDataFolder(), fileName);
		reload();
	}
	
	@Override
	public void set(String key, Object value) {
		if (value instanceof Serializable && !value.getClass().isPrimitive() && !(value instanceof String)) {
			super.set(key, SerializationUitls.getString(value));
		}
		else super.set(key, value);
		save();
	}
	
	/**
	 * @param key The key of the value.
	 * @return The serialized object.
	 */
	@Override
	public Object get(String key) {
		try {
			Object o = SerializationUitls.getObject(getString(key));
			if (o == null) throw new NullPointerException();
			return o;
		} catch (NullPointerException | SerializableError e) {
			return this.getString(key);
		}
	}
	
	/**
	 * Reload configuration
	 */
	public void reload() {
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException exception) {
				throw new GeneralBaseCoreException(exception);
			}
		}
		try {
			load(file);
			if (defaults != null) {
				InputStreamReader reader = new InputStreamReader(plugin.getResource(defaults));
				FileConfiguration defaultsConfig = YamlConfiguration.loadConfiguration(reader);
				setDefaults(defaultsConfig);
				options().copyDefaults(true);
				reader.close();
				save();
			}
		} catch (IOException exception) {
			throw new GeneralBaseCoreException(exception);
		} catch (InvalidConfigurationException exception) {
			throw new GeneralBaseCoreException(exception);
		}
	}

	/**
	 * Save configuration
	 */
	public void save() {
		try {
			options().indent(2);
			save(file);
		} catch (IOException exception) {
			throw new GeneralBaseCoreException(exception);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JSONObject> getJSONList(String key) {
		return (List<JSONObject>) super.getList(key);
	}
	
}

package me.benana.basecore.information;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.json.simple.JSONObject;

import me.benana.basecore.Main;
import me.benana.basecore.information.sql.MySQLDataBase;
import me.benana.basecore.information.sql.SQLDataManager;
import me.benana.basecore.utils.SerializationUitls;

/**
 * SQLSaver is an easy system of saving an information with MySQL. You can use it similar to config. NOT SERIALIZABLE
 * 
 * @author DrBenana
 */
public class SQLSaver implements Saver {
	private SQLDataManager sdm;
	private String container;
	
	/**
	 * @param hostname The hostname of the machine that hosts the MySQL server. If you host it on your local server, you will need to use "localhost"
	 * @param port The port of the MySQL server. The default is "3306"
	 * @param database The name of the Database. 
	 * @param username The username of the Database adminstrator. It's usually "root".
	 * @param password The password of the username.
	 * @param name The name of the "file". Very similar to config.
	 */
	public SQLSaver(String hostname, String port, String database, String username, String password, String name) throws ClassNotFoundException, IOException, SQLException {
		this(new MySQLDataBase(hostname, port, database, username, password), name);
	}
	
	/**
	 * @param msdb MySQLDataBase object. Easy to create, It is just "new MySQLDataBase(Parms...)".
	 * @param name The name of the "file". Very similar to config.
	 */
	public SQLSaver(MySQLDataBase msdb, String name) throws ClassNotFoundException, IOException, SQLException {
		this(new SQLDataManager(msdb, "BaseCore"), name);
	}
	
	/**
	 * @param sdm SQLDataManager object. Easy to create, It is just "new SQLDataManager(Parms...)".
	 * @param name The name of the "file". Very similar to config.
	 */
	public SQLSaver(SQLDataManager sdm, String name) throws ClassNotFoundException, IOException, SQLException {
		this.container = name;
		this.sdm = sdm;
		if (!sdm.valuesInited(container)) sdm.initValuesOf(container);
	}
	
	@SuppressWarnings("unchecked")
	public void set(String key, Object value) {
		if (value instanceof String) sdm.setString(container, key, (String) value);
		else if (value instanceof Integer) sdm.setInt(container, key, (Integer) value);
		else if (value instanceof Double) sdm.setDouble(container, key, (Double) value);
		else if (value instanceof Boolean) sdm.setString(container, key, (Boolean) value ? "true" : "false");
		else if (value instanceof List<?>) {
			JSONObject json = new JSONObject();
			((List<?>) value).forEach(s -> json.put(s, "None"));
			sdm.setJSON(container, key, json);
		}
		else if (value instanceof Serializable) sdm.setString(container, key, SerializationUitls.getString(value));
		else Main.getMessageCore().sendDefault("CantSaveType", Bukkit.getConsoleSender());
	}
	
	@Override
	public String getString(String key) {
		return sdm.getString(container, key);
	}
	
	@Override
	public int getInt(String key) {
		return sdm.getInt(container, key);
	}
	
	@Override
	public double getDouble(String key) {
		return sdm.getDouble(container, key);
	}
	
	@Override
	public boolean getBoolean(String key) {
		return sdm.getString(container, key).equalsIgnoreCase("true") ? true : false;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<?> getList(String key) {
		return new ArrayList<>(sdm.getJSON(container, key).keySet());
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> getStringList(String key) {
		return new ArrayList<>(sdm.getJSON(container, key).keySet());
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Double> getDoubleList(String key) {
		return new ArrayList<>(sdm.getJSON(container, key).keySet());
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Integer> getIntegerList(String key) {
		return new ArrayList<>(sdm.getJSON(container, key).keySet());
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<JSONObject> getJSONList(String key) {
		return new ArrayList<>(sdm.getJSON(container, key).keySet());
	}

	@Override
	public Object get(String key) {
		return SerializationUitls.getObject(getString(key));
	}
}

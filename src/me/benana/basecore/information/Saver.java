package me.benana.basecore.information;

import java.util.List;

import org.json.simple.JSONObject;

public interface Saver {
	
	/**
	 * @param key The path to the value.
	 * @param value The value that the key contains.
	 */
	public void set(String key, Object value);
	
	/**
	 * @param key The path to the value.
	 * @return The object that the key contains.
	 */
	public Object get(String key);
	
	/**
	 * @param key The path to the value.
	 * @return The string that the key contains.
	 */
	public String getString(String key);
	
	/**
	 * @param key The path to the value.
	 * @return The int that the key contains.
	 */
	public int getInt(String key);
	
	/**
	 * @param key The path to the value.
	 * @return The double that the key contains.
	 */
	public double getDouble(String key);
	
	/**
	 * @param key The path to the value.
	 * @return The boolean that the key contains.
	 */
	public boolean getBoolean(String key);
	
	/**
	 * @param key The path to the value.
	 * @return The list that the key contains.
	 */
	public List<?> getList(String key);
	
	/**
	 * @param key The path to the value.
	 * @return The list that the key contains.
	 */
	public List<String> getStringList(String key);
	
	/**
	 * @param key The path to the value.
	 * @return The list that the key contains.
	 */
	public List<Double> getDoubleList(String key);
	
	
	/**
	 * @param key The path to the value.
	 * @return The list that the key contains.
	 */
	public List<Integer> getIntegerList(String key);
	
	
	/**
	 * @param key The path to the value.
	 * @return The list that the key contains.
	 */
	public List<JSONObject> getJSONList(String key);
	
}

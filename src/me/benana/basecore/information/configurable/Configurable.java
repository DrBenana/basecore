package me.benana.basecore.information.configurable;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Put it on class and fields to save
 * 
 * @author DrBenana
 */
@Retention(RUNTIME)
@Target({ TYPE, FIELD })
public @interface Configurable {
	/**
	 * @return The prefix of the path on the config.
	 */
	String prefix() default "";
	/**
	 * @return The name of the saver.
	 */
	String saver() default "";
	/**
	 * @return The name of the field.
	 */
	String name() default "";
}

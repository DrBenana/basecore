package me.benana.basecore.information.configurable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.basecore.BaseCore;
import me.benana.basecore.events.EnableServerEvent;
import me.benana.basecore.information.Saver;
import me.benana.basecore.utils.reflection.FieldUtils;

/**
 * @author DrBenana
 */
public class ConfigurableCore implements Listener {
	private static HashMap<JavaPlugin, ArrayList<Object>> configurables = new HashMap<>();
	
	/**
	 * @param plugin The JavaPlugin instance of your server.
	 * @param objects The objects to save to the config.
	 */
	public static void registerConfigurables(JavaPlugin plugin, Object... objects) {
		for (Object o : objects) {
			if (!configurables.containsKey(plugin)) configurables.put(plugin, new ArrayList<>());
			configurables.get(plugin).add(o);
		}
	}
	
	@EventHandler
	public void onEnable(EnableServerEvent e) {
		BaseCore.debug("Loading configurables");
		for (JavaPlugin pl : configurables.keySet()) {
			for (Object o : configurables.get(pl)) {
				Configurable classconf = null;
				if (o.getClass().isAnnotationPresent(Configurable.class))
					classconf = o.getClass().getAnnotation(Configurable.class);
				else {
					BaseCore.debug("You cannot register a class without a @Configuration on it.");
					continue;
				}
				for (Field f : o.getClass().getFields()) {
					if (f.isAnnotationPresent(Configurable.class)) {
						Configurable c = f.getAnnotation(Configurable.class);
						Saver s = BaseCore.getDefaultSaver(c.saver().equals("") ? classconf.saver() : c.saver(), pl);
						String prefix = c.prefix().equals("") ? classconf.prefix() : c.prefix(), name = c.name().equals("") ? classconf.name() : c.name();
						name = name.equals("") ? f.getName() : name;
						Object value = s.get(prefix + "." + name);
						if (s != null && value != null) {
							FieldUtils.setDeclaredField(o, f.getName(), value);
							BaseCore.debug("Configurable " + f.getName() + " is got the value " + value.toString());
						} else {
							BaseCore.debug("Something is null (" + f.getName() + ")");
						}
					}
				}
			}
		}
		BaseCore.debug("Finished");
	}
	
	public static void onStop() {
		BaseCore.debug("Starting to save configurables");
		for (JavaPlugin pl : configurables.keySet()) {
			for (Object o : configurables.get(pl)) {
				Configurable classconf = null;
				if (o.getClass().isAnnotationPresent(Configurable.class))
					classconf = o.getClass().getAnnotation(Configurable.class);
				else continue;
				for (Field f : o.getClass().getFields()) {
					if (f.isAnnotationPresent(Configurable.class)) {
						if (FieldUtils.getDeclaredField(o, f.getName()) == null) {
							BaseCore.debug(f.getName() + " is null. It has been not saved.");
							continue;
						}
						Configurable c = f.getAnnotation(Configurable.class);
						Saver s = BaseCore.getDefaultSaver(c.saver().equals("") ? classconf.saver() : c.saver(), pl);
						String prefix = c.prefix().equals("") ? classconf.prefix() : c.prefix(), name = c.name().equals("") ? classconf.name() : c.name();
						s.set(prefix + "." + name, FieldUtils.getDeclaredField(o, f.getName()));
						BaseCore.debug(f.getName() + " is saved with the value " + FieldUtils.getDeclaredField(o, f.getName()).toString());
					}
				}
			}
		}
	}
}

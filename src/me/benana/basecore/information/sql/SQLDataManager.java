package me.benana.basecore.information.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.scheduler.BukkitRunnable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import me.benana.basecore.BaseCore;
import me.benana.basecore.Main;
import me.benana.basecore.exceptions.GeneralBaseCoreException;

/**
 * NOT SERIALIZABLE
 * 
 * @author NacOJerk
 *
 */
public class SQLDataManager {
	private MySQLDataBase database = null;
	private Connection c;
	private String tableName;

	private enum ObjType {
		STRING, INTEGER, FLOAT
	}
	
	/**
	 * @param sqlm MySQLDataBase object. You can create it by "new MySQLDataBase(Parms...)".
	 * @param tableName The name of the table. GeneralSaver uses (as default) with "BaseCore".
	 */
	public SQLDataManager(MySQLDataBase sqlm, String tableName)
			throws IOException, SQLException, ClassNotFoundException {
		database = sqlm;
		this.tableName = tableName;
		c = database.openConnection();
		Statement statement = c.createStatement();
		statement.executeUpdate("CREATE TABLE IF NOT EXISTS " + tableName
				+ "(ID int NOT NULL AUTO_INCREMENT, IDENTIFY varchar(255) NOT NULL, PRIMARY KEY (ID));");
	}

	public void initValuesOf(String s) {
		try {
			Statement statement = c.createStatement();
			statement.executeUpdate("INSERT INTO " + tableName + "(IDENTIFY) VALUES('" + s + "');");
		} catch (SQLException e) {
			throw new GeneralBaseCoreException(e);
		}
	}

	public boolean valuesInited(String s) {
		try {
			Statement statement = c.createStatement();
			ResultSet res = statement.executeQuery("SELECT * FROM " + tableName + " WHERE IDENTIFY = '" + s + "';");
			if (!res.next()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			return false;
		}
	}

	public void setString(String s, String path, String data) {
		setPlayerData(s, path.replaceAll("\\'", "GERESH").replaceAll("\\[", "MERUBAL").replaceAll("\\]", "MERUBAR").replaceAll("\\.", "NEKUDA"),
				data.replaceAll("\\'", "GERESH_WAS_HERE_BASECORE").replaceAll("\\'", "GERESH").replaceAll("\\[", "MERUBAL").replaceAll("\\]", "MERUBAR").replaceAll("\\.", "NEKUDA"), ObjType.STRING);
	}

	public void setInt(String s, String path, int data) {
		setPlayerData(s, path, data, ObjType.INTEGER);
	}

	public void setDouble(String s, String path, double data) {
		setPlayerData(s, path, data, ObjType.FLOAT);
	}

	public String getString(String s, String path) {
		return ((String) getPlayerData(s.replaceAll("\\'", "GERESH").replaceAll("\\[", "MERUBAL").replaceAll("\\]", "MERUBAR").replaceAll("\\.", "NEKUDA"),
				path)).replaceAll("GERESH", "\\'").replaceAll("MERUBAL", "\\[").replaceAll("MERUBAR", "\\]").replaceAll("NEKUDA", "\\.");
	}

	public int getInt(String s, String path) {
		return (int) getPlayerData(s, path);
	}

	public double getDouble(String s, String path) {
		return (double) getPlayerData(s, path);
	}

	public boolean containsPath(String s, String path) {
		return getPlayerData(s, path) != null;
	}

	protected void setPlayerData(String s, String key, Object Value, ObjType type) {
		try {
			DatabaseMetaData md = c.getMetaData();
			ResultSet rs = md.getColumns(null, null, tableName, key);
			// False if colomn doesn't exist
			if (!rs.next()) {//
				Statement statement = c.createStatement();
				String SQL = "ALTER TABLE " + tableName + " ADD " + key + " ";
				switch (type) {
				case FLOAT:
					SQL += "float";
					break;
				case INTEGER:
					SQL += "int";
					break;
				case STRING:
					SQL += "varchar(255)";
					break;
				}
				SQL += ";";
				BaseCore.debug(SQL);
				statement.executeUpdate(SQL);
			}
			new BukkitRunnable() {

				@Override
				public void run() {
					Statement statement;
					try {
						statement = c.createStatement();
						statement.executeUpdate("UPDATE " + tableName + " SET " + key + "='" + Value.toString()
								+ "' WHERE IDENTIFY='" + s + "';");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}.runTaskLaterAsynchronously(Main.getPlugin(), 2l);
		} catch (SQLException e) {
			throw new GeneralBaseCoreException(e);
		}
	}

	protected Object getPlayerData(String s, String key) {
		key = key.replaceAll("\\.", "-");
		try {
			Statement statement = c.createStatement();
			ResultSet res = statement.executeQuery("SELECT * FROM " + tableName + " WHERE IDENTIFY = '" + s + "';");
			res.next();
			return res.getObject(key);
		} catch (SQLException e) {
			return null;
		}
	}

	public JSONObject getJSON(String p, String path) {
		JSONParser parser = new JSONParser();
		try {
			return (JSONObject) parser.parse(getString(p, path));
		} catch (ParseException e) {
			return null;
		}
	}

	public void setJSON(String p, String path, JSONObject object) {
		setString(p, path, object.toString());
	}

}

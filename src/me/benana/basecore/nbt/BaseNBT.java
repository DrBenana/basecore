package me.benana.basecore.nbt;

import java.io.Serializable;
import java.security.InvalidParameterException;

import me.benana.basecore.general.Parameter;
import me.benana.basecore.utils.SerializationUitls;
import me.benana.basecore.utils.reflection.ClassUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

/**
 * @author DrBenana
 */
public abstract class BaseNBT {
	
	/**
	 * Sets NBTTag
	 * @param key The path of the tag
	 * @param value The value of the tag
	 */
	public void setTag(String key, Object value) {
		if (value instanceof Boolean) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setBoolean", new Parameter(String.class, key), new Parameter(boolean.class, value));
			setNBT(nbt);
		}
		if (value instanceof Float) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setFloat", new Parameter(String.class, key), new Parameter(float.class, value));
			setNBT(nbt);
		}
		else if (value instanceof Double) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setDouble", new Parameter(String.class, key), new Parameter(double.class, value));
			setNBT(nbt);
		}
		else if (value instanceof Byte) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setByte", new Parameter(String.class, key), new Parameter(byte.class, value));
			setNBT(nbt);
		}
		else if (value instanceof Byte[]) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setByteArray", new Parameter(String.class, key), new Parameter(byte[].class, value));
			setNBT(nbt);
		}
		else if (value instanceof Short) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setShort", new Parameter(String.class, key), new Parameter(short.class, value));
			setNBT(nbt);
		}
		else if (value instanceof Integer) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setInt", new Parameter(String.class, key), new Parameter(int.class, value));
			setNBT(nbt);
		}
		else if (value instanceof Integer[]) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setIntArray", new Parameter(String.class, key), new Parameter(int[].class, value));
			setNBT(nbt);
		}
		else if (value instanceof String) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setString", new Parameter(String.class, key), new Parameter(String.class, value));
			setNBT(nbt);
		}
		else if (value instanceof Serializable) {
			Object nbt = getNBT();
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), nbt, "setString", new Parameter(String.class, key), new Parameter(String.class, SerializationUitls.getString(value)));
			setNBT(nbt);
		}
		else throw new InvalidParameterException("You've entered invalid value to the setTag. You have to look at the documents for the allowed values.");
	}
	
	/**
	 * @param key The path
	 * @return contains the path
	 */
	public boolean contains(String key) {
		return (boolean) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "hasKey", new Parameter(String.class, key));
	}
	
	public void remove(String key) {
		ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "remove", new Parameter(String.class, key));
	}
	
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public int getInt(String key) {
		return (int) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getInt", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public int[] getArrayInt(String key) {
		return (int[]) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getIntArray", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public byte getByte(String key) {
		return (byte) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getByte", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public byte[] getArrayByte(String key) {
		return (byte[]) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getByteArray", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public short getShort(String key) {
		return (short) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getShort", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public double getDouble(String key) {
		return (double) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getDouble", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public float getFloat(String key) {
		return (float) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getFloat", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public String getString(String key) {
		return (String) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getString", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the tag
	 * @return The value of the tag
	 */
	public boolean getBoolean(String key) {
		return (boolean) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("NBTTagCompound"), getNBT(), "getBoolean", new Parameter(String.class, key));
	}
	/**
	 * @param key The path of the serialized tag
	 * @return The value of the unserialized tag
	 */
	public Object getSerializedTag(String key) {
		return SerializationUitls.getObject(getString(key));
	}
	
	/**
	 * @return NBTCompound object
	 */
	public abstract Object getNBT();
	
	/**
	 * @param nbt Full NBTCompound object to set
	 */
	public abstract void setNBT(Object nbt);
	
	/**
	 * @return The final object to manipulate.
	 */
	public abstract Object getFinalObject();
	
}

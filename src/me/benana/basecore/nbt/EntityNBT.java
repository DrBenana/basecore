package me.benana.basecore.nbt;

import org.bukkit.entity.Entity;

import me.benana.basecore.general.Parameter;
import me.benana.basecore.utils.reflection.ClassUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

/**
 * @author DrBenana
 */
public class EntityNBT extends BaseNBT {
	private final Entity e;
	
	/**
	 * @param e LivingEntity to manipulate the NBT for.
	 */
	public EntityNBT(Entity e) {
		this.e = e;
	}
	
	@Override
	public Object getNBT() {
		Object eNMS = NMSUtils.asNMSObject(e);
		Object nbt = ClassUtils.invokeConstructor(NMSUtils.getNMS("NBTTagCompound"));
		ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("Entity"), eNMS, "c", new Parameter(NMSUtils.getNMS("NBTTagCompound"), nbt));
		if (nbt == null) nbt = ClassUtils.invokeConstructor(NMSUtils.getNMS("NBTTagCompound"));
		return nbt;
	}

	@Override
	public void setNBT(Object nbt) {
		Object eNMS = NMSUtils.asNMSObject(e);
		ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("EntityLiving"), eNMS, "a", new Parameter(NMSUtils.getNMS("NBTTagCompound"), nbt));
	}

	@Override
	public Object getFinalObject() {
		return e;
	}

}

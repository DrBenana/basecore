package me.benana.basecore.nbt;

import org.bukkit.inventory.ItemStack;

import me.benana.basecore.general.Parameter;
import me.benana.basecore.utils.reflection.ClassUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

/**
 * @author DrBenana
 */
public class ItemNBT extends BaseNBT {
	private ItemStack is;
	
	/**
	 * @param is ItemStack to manipulate the NBT for.
	 */
	public ItemNBT(ItemStack item) {
		this.is = item;
	}

	@Override
	public Object getNBT() {
		Object o = null;
		if ((boolean) ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("ItemStack"), NMSUtils.asNMSItemStack(is), "hasTag"))
			o = ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("ItemStack"), NMSUtils.asNMSItemStack(is), "getTag");
		if (o == null) o = ClassUtils.invokeConstructor(NMSUtils.getNMS("NBTTagCompound"));
		return o;
	}

	@Override
	public void setNBT(Object nbt) {
		Object nmsItem = NMSUtils.asNMSItemStack(is);
		ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("ItemStack"), nmsItem, "setTag", new Parameter(NMSUtils.getNMS("NBTTagCompound"), nbt));
		is = NMSUtils.asBukkitItemStack(nmsItem);
	}

	@Override
	public Object getFinalObject() {
		return is;
	}
	
}

package me.benana.basecore.nbt;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

/**
 * @author DrBenana
 */
public class NBT extends BaseNBT {
	private BaseNBT nbt;
	
	/**
	 * @param e LivingEntity to manipulate the NBT for.
	 */
	public NBT(Entity e) {
		nbt = new EntityNBT(e);
	}
	
	/**
	 * @param is ItemStack to manipulate the NBT for.
	 */
	public NBT(ItemStack is) {
		nbt = new ItemNBT(is);
	}
	
	/**
	 * @param b Block[TILE!] to manipulate the NBT for.
	 */
	public NBT(Block b) {
		nbt = new TileNBT(b);
	}

	@Override
	public Object getNBT() {
		return nbt.getNBT();
	}

	@Override
	public void setNBT(Object nbt) {
		this.nbt.setNBT(nbt);
	}

	@Override
	public Object getFinalObject() {
		return nbt.getFinalObject();
	}
	
	
}

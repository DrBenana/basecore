package me.benana.basecore.nbt;

import org.bukkit.block.Block;

import me.benana.basecore.general.Parameter;
import me.benana.basecore.utils.reflection.ClassUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

/**
 * @author DrBenana
 */
public class TileNBT extends BaseNBT {
	private final Object tileEntity;
	
	/**
	 * @param b Block[Tile!] to manipulate the NBT for.
	 */
	public TileNBT(Block b) {
		Object position = ClassUtils.invokeConstructor(NMSUtils.getNMS("BlockPosition"), new Parameter(int.class, b.getLocation().getBlockX())
				, new Parameter(int.class, b.getLocation().getBlockY())
				, new Parameter(int.class, b.getLocation().getBlockZ()));
		tileEntity = ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("World"), NMSUtils.asNMSObject(b.getWorld()), "getTileEntity", new Parameter(NMSUtils.getNMS("BlockPosition"), position));
	}
	
	@Override
	public Object getNBT() {
		Object nbt = ClassUtils.invokeConstructor(NMSUtils.getNMS("NBTTagCompound"));
		if (ClassUtils.hasMethod(NMSUtils.getNMS("TileEntity"), "b"))
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("TileEntity"), tileEntity, "b", new Parameter(NMSUtils.getNMS("NBTTagCompound"), nbt));
		else
			ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("TileEntity"), tileEntity, "save", new Parameter(NMSUtils.getNMS("NBTTagCompound"), nbt));
		if (nbt == null) nbt = ClassUtils.invokeConstructor(NMSUtils.getNMS("NBTTagCompound"));
		return nbt;
	}

	@Override
	public void setNBT(Object nbt) {
		ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("TileEntity"), tileEntity, "a", new Parameter(NMSUtils.getNMS("NBTTagCompound"), nbt));
	}

	@Override
	public Object getFinalObject() {
		return tileEntity;
	}
}

package me.benana.basecore.utils;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * General utils for Bukkit.
 * 
 * @author DrBenana
 */
public class BukkitUtils {
	
	/**
	 * @param pl The plugin to register the listeners to.
	 * @param listeners Register all the listeners.
	 */
	public static void registerEvents(JavaPlugin pl, Listener... listeners) {
		for (Listener l : listeners) pl.getServer().getPluginManager().registerEvents(l, pl);
	}
	
}

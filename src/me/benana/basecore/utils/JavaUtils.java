package me.benana.basecore.utils;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import me.benana.basecore.BaseCore;

import java.util.Objects;
import java.util.Random;

/**
 * Utils for general stuff on java.
 *
 * @author DrBenana
 */
public class JavaUtils {
	
	/**
	 * @param map A map to get the kit from.
	 * @param value The value
	 * @return Key of the value
	 */
	public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
	    for (Entry<T, E> entry : map.entrySet()) {
	        if (Objects.equals(value, entry.getValue())) {
	            return entry.getKey();
	        }
	    }
	    return null;
	}
	
	/**
	 * @param a First Collection
	 * @param b Second Collection
	 * @return Do the maps have something in common.
	 */
	public static boolean containsAny(Collection<?> a, Collection<?> b) {
		for (Object c : a)
			if (b.contains(c)) return true;
		return false;
	}
	
	/**
	 * @param a The divide.
	 * @param b The divided.
	 * @return A random chance.
	 */
	public static boolean chance(int a, int b) {
		int c = new Random().nextInt(b)+1;
		BaseCore.debug("The chance is from " + a + " to " + b + ". The rand is " + c + " so the answer is " + (a >= c) + ".");
		return a >= c;
	}
	
	/**
	 * Checks if the number is found between another 2 numbers.
	 * @param n1 The first number.
	 * @param n2 The second number.
	 * @param a The number to check if it found between the another 2 numbers.
	 * @return Boolean
	 */
	public static boolean containsNumber(double n1, double n2, double a) {		
		if (a > n1 && a < n2) return true;
		else if (a < n1 && a > n2) return true;
		else return false;
	}
	
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) { return false; }
	}
	
	public static boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
			return true;
		} catch (Exception e) { return false; }
	}
	
	public static String getRandomString(int length, String chars) {
        String SALTCHARS = chars;
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
	
	public static String getRandomString(int length) {
		return getRandomString(length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
	}
	
	public static String getRandomString() {
		return getRandomString(18);
	}
}

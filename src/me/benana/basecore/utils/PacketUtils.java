package me.benana.basecore.utils;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.benana.basecore.exceptions.ReflectionException;
import me.benana.basecore.general.Parameter;
import me.benana.basecore.utils.reflection.ClassUtils;
import me.benana.basecore.utils.reflection.FieldUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

/**
 * Easily send packets.
 * 
 * @author DrBenana
 */
public class PacketUtils {	
	private static Random rand = new Random();
	
	/**
	 * @param o The packet.
	 * @return The name of the packet as a String.
	 */
	public static String getPacketType(Object o) {
		return o.getClass().getName().replace("net.minecraft.server." + NMSUtils.getVersion() + ".", "");
	}
	
	// BlockBreakAnimation
	
	/**
	 * Sends a packet of 'BlockBreakAnimation' to the player.
	 * @param p The player to send the packet to.
	 * @param x The x location.
	 * @param y The y location.
	 * @param z The z location.
	 */
	public static void sendBlockBreakAnimation(Player p, double x, double y, double z, int amount) {
		try {
			Object position = NMSUtils.getNMS("BlockPosition").getDeclaredConstructor(double.class, double.class, double.class).newInstance(x, y, z);
			Object packet = NMSUtils.getNMS("PacketPlayOutBlockBreakAnimation").getDeclaredConstructor(int.class, position.getClass(), int.class).newInstance(rand.nextInt(2000)+1000, position, amount);			
			PlayerUtils.sendPacket(p, packet);
		} catch (Exception e) {
			throw new ReflectionException(e);
		};
	}
	
	/**
	 * Sends a packet of 'BlockBreakAnimation' to the player.
	 * @param p The player to send the packet to.
	 * @param loc The location. (The world is taken from the player)
	 */
	public static void sendBlockBreakAnimation(Player p, Location loc, int amount) {
		sendBlockBreakAnimation(p, loc.getX(), loc.getY(), loc.getZ(), amount);
	}
	
	// PlayerListHeaderFooter
	
	/**
	 * Sends a packet of 'PlayerListHeaderFooter' to the player.
	 * @param p The player to send the packet to.
	 * @param header The header as a String, May include colors, Mustn't include JSON.
	 * @param footer The footer as a String, May include colors, Mustn't include JSON.
	 */
	public static void sendListHeaderAndFooter(Player p, String header, String footer) {
		Object packet = ClassUtils.invokeConstructor(NMSUtils.getNMS("PacketPlayOutPlayerListHeaderFooter"), new Parameter(NMSUtils.getNMS("IChatBaseComponent"), NMSUtils.getNMSChatComponent(header)));
		FieldUtils.setDeclaredField(packet, "b", NMSUtils.getNMSChatComponent(footer));
		PlayerUtils.sendPacket(p, packet);
	}
	
	// SendActionBar
	
	/**
	 * @param p The player to send the packet to.
	 * @param message The message to set in the action bar.
	 */
	public static void sendActionBar(Player p, String message) {
		Object packet = ClassUtils.invokeConstructor(NMSUtils.getNMS("PacketPlayOutChat"),
				new Parameter(NMSUtils.getNMS("IChatBaseComponent"), NMSUtils.getNMSChatComponent(message)),
				new Parameter(byte.class, (byte) 2));
		PlayerUtils.sendPacket(p, packet);
	}

	// SendTitle
	
	/**
	 * @param p The player to send the packet to.
	 * @param title The title to send to the player.
	 */
	public static void sendTitle(Player p, String title) {
		sendTitle(p, title, 0, 20, 0);
	}
	
	/**
	 * @param p The player to send the packet to.
	 * @param title The title to send to the player.
	 * @param subtitle The subtitle to send to the player.
	 */
	public static void sendTitle(Player p, String title, String subtitle) {
		sendTitle(p, title, subtitle, 0, 20, 0);
	}
	
	/**
	 * @param p The player to send the packet to.
	 * @param message The title to send to the player.
	 * @param fadeIn A time to fadeIn [Ticks]
	 * @param stay A time to stay [Ticks]
	 * @param fadeOut A time to fadeOut [Ticks]
	 */
	public static void sendTitle(Player p, String title, int fadeIn, int stay, int fadeOut) {
		Object EnumAction = ClassUtils.invokeDeclaredMethod(
				NMSUtils.getNMS("EnumTitleAction"),
				(Object) null, "a", new Parameter("TITLE"));
		Object packet = ClassUtils.invokeConstructor(NMSUtils.getNMS("PacketPlayOutTitle"),
				new Parameter(NMSUtils.getNMS("EnumTitleAction"), EnumAction),
				new Parameter(NMSUtils.getNMS("IChatBaseComponent"), NMSUtils.getNMSChatComponent(title)),
				new Parameter(int.class, fadeIn), new Parameter(int.class, stay), new Parameter(int.class, fadeOut));
		PlayerUtils.sendPacket(p, packet);
	}
	
	/**
	 * @param p The player to send the packet to.
	 * @param title The title to send to the player.
	 * @param subtitle The subtitle to send to the player.
	 * @param fadeIn A time to fadeIn [Ticks]
	 * @param stay A time to stay [Ticks]
	 * @param fadeOut A time to fadeOut [Ticks]
	 */
	public static void sendTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		Object EnumAction = ClassUtils.invokeDeclaredMethod(
				NMSUtils.getNMS("EnumTitleAction"),
				(Object) null, "a", new Parameter("SUBTITLE"));
		Object packet = ClassUtils.invokeConstructor(NMSUtils.getNMS("PacketPlayOutTitle"),
				new Parameter(NMSUtils.getNMS("EnumTitleAction"), EnumAction),
				new Parameter(NMSUtils.getNMS("IChatBaseComponent"), NMSUtils.getNMSChatComponent(subtitle)),
				new Parameter(int.class, fadeIn), new Parameter(int.class, stay), new Parameter(int.class, fadeOut));
		PlayerUtils.sendPacket(p, packet);
		sendTitle(p, title, fadeIn, stay, fadeOut);
	}

}

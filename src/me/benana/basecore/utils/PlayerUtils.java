package me.benana.basecore.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;

import me.benana.basecore.BaseCore;
import me.benana.basecore.exceptions.ReflectionException;
import me.benana.basecore.utils.reflection.FieldUtils;
import me.benana.basecore.utils.reflection.NMSUtils;

/**
 * PlayerUtils contains very nice methods to use with players.
 * 
 * @author DrBenana
 */
public class PlayerUtils {
	public static final ArrayList<String> fakeNames = new ArrayList<>();
	private static final HashMap<UUID, PlayerUtils> playerutils = new HashMap<>();
	private final UUID uuid;
	private static final HashMap<String, UUID> realname = new HashMap<>();
	private String nameAboveHead;
	
	/**
	 * @param player The player to enable the utils on.
	 */
	public PlayerUtils(Player player) {
		uuid = player.getUniqueId();
		nameAboveHead = playerutils.containsKey(uuid) ? playerutils.get(uuid).getNameAboveHead() : player.getName();
	}
	
	/**
	 * @return The player of the utils. @Nullable when the player is offline.
	 */
	public Player getPlayer() {
		return Bukkit.getPlayer(uuid);
	}
	
	/**
	 * @param name A name to set everywhere.
	 * @return PlayerUtils instance.
	 */
	public PlayerUtils setDisplayName(String name) {
		setNameAboveHead(name);
		getPlayer().setDisplayName(name);
		getPlayer().setPlayerListName(name);
		return this;
	}
	
	/**
	 * @param name A name to set above the head of the player.
	 * @return PlayerUtils instance.
	 */
	public PlayerUtils setNameAboveHead(String name) {
		if (getPlayer() == null) {
			BaseCore.debug("The player is offline so the method 'PlayerUtils#setNameAboveHead' won't run.");
			return this;
		}
		try {
			fakeNames.remove(getPlayer().getName());
			GameProfile profile = (GameProfile) getPlayer().getClass().getMethod("getProfile").invoke(getPlayer());
			Field nameField = profile.getClass().getDeclaredField("name");
	        nameField.setAccessible(true);
	        Field fieldModifier = nameField.getClass().getDeclaredField("modifiers");
	        fieldModifier.setAccessible(true);
	        fieldModifier.set(nameField, nameField.getModifiers() & ~Modifier.FINAL);
	        nameField.set(profile, name);
	        Bukkit.getOnlinePlayers().forEach(p -> {
	        	if (!p.equals(getPlayer())) {
	        		p.hidePlayer(getPlayer());
	        		p.showPlayer(getPlayer());
	        	}
	        });
	        this.nameAboveHead = name;
	        fakeNames.add(name);
		} catch (Exception ex) { throw new ReflectionException(ex); }
		playerutils.put(uuid, this);
		return this;
	}
	
	/**
	 * @return The name above the head.
	 */
	public String getNameAboveHead() {
		return nameAboveHead;
	}
	
	/**
	 * @param packet The packet to send to the player.
	 */
	public void sendPacket(Object packet) {
		sendPacket(getPlayer(), packet);
	}
	
	/**
	 * @param p The player to send the packet to.
	 * @param packet The packet.
	 */
	public static void sendPacket(Player p, Object packet) {
		Object conn = FieldUtils.getDeclaredField(NMSUtils.asNMSObject(p), "playerConnection");
		try {
			conn.getClass().getDeclaredMethod("sendPacket", NMSUtils.getNMS("Packet")).invoke(conn, packet);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			throw new ReflectionException(e);
		}
	}
	
	/**
	 * @param uuid The uuid of the player.
	 * @param name The real name.
	 */
	public static void setRealName(UUID uuid, String name) {
		realname.put(name, uuid);
	}
	
	/**
	 * @param p The player.
	 * @param name The real name.
	 */
	public static void setRealName(Player p, String name) {
		setRealName(p.getUniqueId(), name);
	}
	
	/**
	 * @param uuid The UUID of the player
	 * @return The real name
	 */
	public static String getRealName(UUID uuid) {
		return JavaUtils.getKeyByValue(realname, uuid);
	}
	
	/**
	 * @param p The player
	 * @return The real name of him
	 */
	public static String getRealName(Player p) {
		return getRealName(p.getUniqueId());
	}
	
	/**
	 * @return Cloned HashMap of the PlayerUtils.
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<UUID, PlayerUtils> getClonedPlayerUtilsMap() {
		return (HashMap<UUID, PlayerUtils>) playerutils.clone();
	}
	
	/**
	 * @return Cloned HashMap of the RealName.
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, UUID> getClonedRealNameMap() {
		return (HashMap<String, UUID>) realname.clone();
	}
}

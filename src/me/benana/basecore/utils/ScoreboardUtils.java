package me.benana.basecore.utils;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

import me.benana.basecore.BaseCore;
import me.benana.basecore.Main;

/**
 * ScoreboardUtils allows you to create a new Scoreboard easily.
 * 
 * @author DrBenana
 */
public class ScoreboardUtils {
	private static final HashMap<UUID, ScoreboardUtils> scoreboards = new HashMap<>();
	private final Player p;
	private boolean enable = true;
	
	/**
	 * @param player The player to create the scoreboard to.
	 * @param header The header (title) of the scoreboard.
	 * @param display_slots The slots to display the scoreboard in.
	 */
	public ScoreboardUtils(Player player, String header, DisplaySlot... display_slots) {
		p = player;
		scoreboards.put(p.getUniqueId(), this);
		if (!Main.getPlugin().getConfig().getBoolean("Scoreboard.Enable")) {
			BaseCore.debug("Scoreboard is disabled via the config of BaseCore. The scoreboard won't create for " + player.getName() + ".");
			enable = false;
			return;
		}
		if (Main.getPlugin().getConfig().getStringList("Scoreboard.BlockTitle").contains(ChatColor.stripColor(header))) {
			BaseCore.debug("The title " + header + " is blocked via the config. The scoreboard won't create for " + player.getName() + ".");
			enable = false;
			return;
		}
		p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		Objective obj = p.getScoreboard().registerNewObjective("basecore", "dummy");
		obj.setDisplayName(header);
		for (DisplaySlot ds : display_slots) obj.setDisplaySlot(ds);
		BaseCore.debug("Scoreboard has been created for the player " + p.getName() + ".");
	}

	/**
	 * @return The player who uses the ScoreboardUtils.
	 */
	public Player getPlayer() {
		return p;
	}
	
	/**
	 * @param line The line to add to the scoreboard.
	 * @return Instance of ScoreboardUtils.
	 */
	public ScoreboardUtils setLine(String line) {
		if (!enable) return this;
		return setLine(line, highestScore()+1);
	}
	
	/**
	 * @param line The line to set to the scoreboard.
	 * @param num The number (score) of the line. LOWEST: 1
	 * @return Instance of ScoreboardUtils.
	 */
	public ScoreboardUtils setLine(String line, int num) {
		if (!enable) return this;
		Score s = getScore(num);
		if (s != null) p.getScoreboard().resetScores(s.getEntry());
		p.getScoreboard().getObjective("basecore").getScore(line).setScore(num);
		return this;
	}
	
	/**
	 * @param line The line to replace on the scoreboard.
	 * @param old The old string (startsWith).
	 * @return Instance of ScoreboardUtils.
	 */
	public ScoreboardUtils setLine(String line, String old) {
		if (!enable) return this;
		Score s = getScore(old);
		if (s != null) p.getScoreboard().resetScores(s.getEntry());
		p.getScoreboard().getObjective("basecore").getScore(line).setScore(s == null ? highestScore() : s.getScore());
		return this;
	}
	
	/**
	 * Get a score that starts with the line.
	 * @param line The line.
	 * @return The score.
	 */
	public Score getScore(String line) {
		if (!enable) return null;
		if (p.getScoreboard() != null && p.getScoreboard().getObjective("basecore") != null)
			for (String s : p.getScoreboard().getEntries()) 
				if (s.startsWith(line)) return p.getScoreboard().getScores(s).iterator().next();
		return null;
	}
	
	/**
	 * Get a score that equals to the line.
	 * @param line The line.
	 * @return The score.
	 */
	public Score getExactScore(String line) {
		if (!enable) return null;
		if (p.getScoreboard() != null && p.getScoreboard().getObjective("basecore") != null)
			for (String s : p.getScoreboard().getEntries()) 
				if (s.equals(line)) return p.getScoreboard().getScores(s).iterator().next();
		return null;
	}
	
	/**
	 * Get a score with the number of socre.
	 * @param line The number.
	 * @return The score.
	 */
	public Score getScore(int num) {
		if (!enable) return null;
		if (p.getScoreboard() != null && p.getScoreboard().getObjective("basecore") != null)
			for (String s : p.getScoreboard().getEntries()) 
				if (p.getScoreboard().getScores(s).iterator().next().getScore() == num) return p.getScoreboard().getScores(s).iterator().next();
		return null;
	}
	
	/**
	 * @return The highest score on the scoreboard. May be 0.
	 */
	public int highestScore() {
		if (!enable) return 0;
		int high = 0;
		if (p.getScoreboard() != null && p.getScoreboard().getObjective("basecore") != null)
			for (String s : p.getScoreboard().getEntries())
				if (p.getScoreboard().getScores(s).iterator().next().getScore() > high) high = p.getScoreboard().getScores(s).iterator().next().getScore();
		return high;
	}
	
	/**
	 * Removes the last scoreboard from the player.
	 */
	public void destroyScoreboard() {
		p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		scoreboards.remove(p.getUniqueId());
	}
	
	/**
	 * Gets the last ScoreboardUtils of the player. If it's the first time that the player tries to get utils, It creates a default scoreboard. <br />
	 * It's not recommended to use this features but if you know that you're the only plugin who uses ScoreboardUtils.
	 * @param p The player
	 * @return Instance of ScoreboardUtils.
	 */
	public static ScoreboardUtils getUtils(Player p) {
		BaseCore.debug("Trying to get the last scoreboard of the player.");
		return scoreboards.containsKey(p.getUniqueId()) ? scoreboards.get(p.getUniqueId()) : new ScoreboardUtils(p, "BaseCore", DisplaySlot.SIDEBAR);
	}
}

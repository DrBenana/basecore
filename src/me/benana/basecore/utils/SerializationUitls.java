package me.benana.basecore.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

import me.benana.basecore.BaseCore;
import me.benana.basecore.exceptions.SerializableError;

/**
 * SerializableUtils allows you to get Serializable objects as a String and hash them into String.
 * 
 * @author DrBenana
 */
public class SerializationUitls {
	
	/**
	 * @param object The object to get a String from.
	 * @return String of the byte codes of the serialized object.
	 */
	@SuppressWarnings("resource")
	public static String getString(Object object) {
		try {
			// Output Stream
			BaseCore.debug("Tries to get a string from a serializabled object by creating a new file with the name 'temp.bc'");
			FileOutputStream fos = new FileOutputStream("temp.bc");
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(object);
			out.close();
			fos.close();
			
			// Read file
			FileInputStream fis = new FileInputStream("temp.bc");
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader buf = new BufferedReader(isr);
			StringBuilder sb = new StringBuilder();
			String line;
			while(( line = buf.readLine()) != null ) {
				sb.append( line );
				sb.append( '\n' );
			}
			BaseCore.debug("The final string is:");
			BaseCore.debug(sb.toString());
			return sb.toString();
		} catch (IOException e) {
			throw new SerializableError("Coudn't serialize the object.");
		}
	}
	
	/**
	 * @param btc String of the byte codes of the serialized object. 
	 * @return The serialized object to get from the String.
	 */
	public static Object getObject(String btc) {
		try {
			// Output Stream
			BaseCore.debug("Tries to create a new file with the name 'temp.bc' and write a string of serializabled object into it. The String:");
			BaseCore.debug(btc);
			PrintWriter out = new PrintWriter("temp.bc");
			out.write(btc);
			out.close();
			
			// Input Stream
			FileInputStream fis = new FileInputStream("temp.bc");
			ObjectInputStream in = new ObjectInputStream(fis);
			Object object = in.readObject();
			if (object == null) BaseCore.debug("The object is null.");
			else BaseCore.debug("The object is created successfuly.");
			in.close();
			fis.close();
			return object;
		} catch (IOException | ClassNotFoundException e) {
			throw new SerializableError("Coudn't deserialize the object.");
		}
	}
	
}

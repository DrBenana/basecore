package me.benana.basecore.utils.reflection;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.WorldType;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.inventory.ItemStack;

import me.benana.basecore.general.BaseCode;
import me.benana.basecore.general.Parameter;

/**
 * Utils to use NMS.
 * 
 * @author DrBenana
 */
public class NMSUtils {
	
	/**
	 * @return Version of the server
	 */
	public static String getVersion() {
		return Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
	}
	
	/**
	 * @param nms The name of the NMS class
	 * @return The class
	 */
	public static Class<?> getNMS(String nms) {
		return ClassUtils.getClass("net.minecraft.server." + getVersion() + "." + nms);
	}
	
	/**
	 * @param nms The name of the NMS class
	 * @return The class of the array of the NMS class
	 */
	public static Class<?> getNMSArray(String nms) {
		return ClassUtils.getArrayClass("net.minecraft.server." + getVersion() + "." + nms);
	}
	
	/**
	 * @param OBC The name of the OBC class
	 * @return The class
	 */
	public static Class<?> getOBC(String OBC) {
		return ClassUtils.getClass("org.bukkit.craftbukkit." + getVersion() + "." + OBC);
	}
	
	/**
	 * @param player The player to convert to the [NMS]EntityPlayer.
	 * @return The player as an entity player.
	 */
	public static Object asNMSObject(Object obj) {
		return ClassUtils.invokeDeclaredMethod(obj, "getHandle");
	}
	
	/**
	 * @param msg The string msg
	 * @return IChatBaseComponent of the msg.
	 */
	public static Object getNMSChatComponent(String msg) {
		if (getNMS("ChatSerializer") == null)
			return ClassUtils.getFullDeclaredObject(getNMS("IChatBaseComponent"), (Object) null, "ChatSerializer.a(*)", new Parameter("{\"text\":\"" + msg + "\"}"));
		else
			return ClassUtils.invokeDeclaredMethod(getNMS("ChatSerializer"), (Object) null, "a", new Parameter("{\"text\":\"" + msg + "\"}"));
	}
	
	/**
	 * @param d The difficulty
	 * @return The difficulty as NMS EnumDifficulty
	 */
	public static Object getEnumDifficulty(Difficulty d) {
		int i = 0;
		switch(d) {
		case EASY:
			i = 1;
			break;
		case NORMAL:
			i = 2;
			break;
		case HARD:
			i = 3;
			break;
		case PEACEFUL:
			i = 0;
			break;
		default:
			break;
		}
		return ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("EnumDifficulty"), (Object) null, "getById", new Parameter(int.class, i));
	}
	
	/**
	 * @param d The gamemode
	 * @return The gamemode as NMS EnumGamemode
	 */
	public static Object getEnumGamemode(GameMode gm) {
		int i = 0;
		switch(gm) {
		case SURVIVAL:
			i = 0;
			break;
		case CREATIVE:
			i = 1;
			break;
		case ADVENTURE:
			i = 2;
			break;
		case SPECTATOR:
			i = 3;
			break;
		default:
			break;
		}
		return ClassUtils.invokeDeclaredMethod(NMSUtils.getNMS("EnumGamemode"), (Object) null, "getById", new Parameter(int.class, i));
	}
	
	/**
	 * @param wt The type of the world
	 * @return The type as a NMS
	 */
	public static Object getNMSWorldType(WorldType wt) {
		switch(wt) {
		case NORMAL:
			return FieldUtils.getDeclaredField(NMSUtils.getNMS("WorldType"), (Object) null, "NORMAL");
		case FLAT:
			return FieldUtils.getDeclaredField(NMSUtils.getNMS("WorldType"), (Object) null, "FLAT");
		case LARGE_BIOMES:
			return FieldUtils.getDeclaredField(NMSUtils.getNMS("WorldType"), (Object) null, "LARGE_BIOMES");
		case AMPLIFIED:
			return FieldUtils.getDeclaredField(NMSUtils.getNMS("WorldType"), (Object) null, "AMPLIFIED");
		case CUSTOMIZED:
			return FieldUtils.getDeclaredField(NMSUtils.getNMS("WorldType"), (Object) null, "CUSTOMIZED");
		case VERSION_1_1:
			return FieldUtils.getDeclaredField(NMSUtils.getNMS("WorldType"), (Object) null, "NORMAL_1_1");
		default:
			return FieldUtils.getDeclaredField(NMSUtils.getNMS("WorldType"), (Object) null, "NORMAL");
		}
	}
	
	/**
	 * @param is The bukkit ItemStack
	 * @return The NMS ItemStack
	 */
	public static Object asNMSItemStack(ItemStack is) {
		return ClassUtils.invokeDeclaredMethod(getOBC("inventory.CraftItemStack"), (Object) null, "asNMSCopy", new Parameter(ItemStack.class, is));
	}
	
	/**
	 * @param is The NMS ItemStack
	 * @return The Bukkit ItemStack
	 */
	public static ItemStack asBukkitItemStack(Object is) {
		return (ItemStack) ClassUtils.invokeDeclaredMethod(getOBC("inventory.CraftItemStack"), (Object) null, "asBukkitCopy", new Parameter(NMSUtils.getNMS("ItemStack"), is));
	}
	
	
	/**
	 * @return SimpleCommandMap
	 */
	public static SimpleCommandMap getCommandMap() {
		return (SimpleCommandMap) ClassUtils.invokeDeclaredMethod(getOBC("CraftServer"), Bukkit.getServer(), "getCommandMap");
	}
	
	public static void registerCommand(BaseCode code, String label, List<String> aliases, String description, String usage, String permission) {
		if (label == null) throw new NullPointerException("Label cannot be null");
		BukkitCommand cmd = new BukkitCommand(label) {
			@Override
			public boolean execute(CommandSender arg0, String arg1, String[] arg2) {
				code.run();
				return false;
			}
		};
		if (aliases != null) cmd.setAliases(aliases);
		if (description != null) cmd.setDescription(description);
		if (usage != null) cmd.setUsage(usage);
		if (permission != null) cmd.setPermission(permission);
		NMSUtils.getCommandMap().register(label, cmd);
	}
	
}
